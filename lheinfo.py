#! /usr/bin/env python

import sys, os, copy
from math import sqrt
from subprocess import Popen
from optparse import OptionParser
#import lighthisto
import math
import gzip

def getTemplate(basename):
    with open('%s.template' % basename, 'r') as f:
        templateText = f.read()
    return string.Template( templateText )

parser = OptionParser(usage="%prog [.lhe file]")

opts, args = parser.parse_args()

if len(args) < 1:
    sys.stderr.write("An .lhe file must be specified\n")
    sys.exit(1)


initial_states_proc = {}
initial_states_info = {}
procid_list = []
nevents = {}
    
# begin reading file:

for lhefile in args:
    infile = gzip.open(lhefile,"rt")
    print('Analyzing', lhefile)
    print('Checking whether LesHouchesEvents tag exists...')
    linecount = 0
    for line in infile:
        linecount = linecount+1
        if '<LesHouchesEvents' in line and linecount==1:
            print('LesHouchesEvents tag found!\n')
            break
        else:
            print('LesHouchesEvents tag not found!')
            exit
    linecount = 0
    linecount_eventfileinfo = 0
    linecount_event = 0
    reading_header = False
    reading_eventfileinfo = False
    reading_event = False
    for line in infile:
        linecount = linecount+1
        if '</init>' in line:
            reading_eventfileinfo = False
        if reading_eventfileinfo is True:
            linecount_eventfileinfo = linecount_eventfileinfo+1
            if linecount_eventfileinfo == 1:
                if len(line.split()) < 4:
                    print('<init> block does not contain proper initial beam info!')
                    exit
                print('incoming hadrons:')
                print('\t', line.split()[0], line.split()[1])
                print('collision energy:')
                print('\t', line.split()[2], line.split()[3])
        if '<init>' in line:
            reading_header = False
            reading_eventfileinfo = True
        if '<LesHouchesEvents' in line:
            reading_header = True
        if reading_event:
            linecount_event = linecount_event+1
            if linecount_event == 1:
                procid = line.split()[0]
                if procid not in procid_list:
                        procid_list.append(procid)
                        nevents[procid] = 0
                        initial_states_info[procid] = {}
                        initial_states_info[procid][0] = 0
                        initial_states_info[procid][1] = 0
                        initial_states_info[procid][2] = 0
            if linecount_event == 2 and line.split()[1] == '-1':
                idin1 = line.split()[0]
                #print idin1 
            if linecount_event == 3 and line.split()[1] == '-1':
                idin2 = line.split()[0]
                #print idin2
        if '</event>' in line:
            nevents[procid] = nevents[procid] + 1
            if idin1 == '21' and idin2 == '21':
                istate = 0
            if idin1 != '21' and idin2 == '21':
                istate = 1
            if idin2 != '21' and idin1 == '21':
                istate = 1
            if idin2 != '21' and idin1 != '21':
                istate = 2
            initial_states_info[procid][istate] = initial_states_info[procid][istate] + 1
            reading_event = False
            linecount_event = 0
            idin1 = -100
            idin2 = -100
            procid = '-100'
        if '<event>' in line:
            reading_event = True

print('\nEvent file analysis:')
for procs in procid_list:
    print('\tprocess id:', procs)
    print('\t\ttotal number of events:')
    print('\t\t\t',nevents[procs])
    print('\t\tinitial state breakdown:')
    print('\t\t\tgg', initial_states_info[procs][0])
    print('\t\t\tqg', initial_states_info[procs][1])
    print('\t\t\tqq', initial_states_info[procs][2])
