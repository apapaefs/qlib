#! /usr/bin/env python

import sys, os, copy
from math import sqrt
from subprocess import Popen
from optparse import OptionParser
import lighthisto

def getTemplate(basename):
    with open('%s.template' % basename, 'r') as f:
        templateText = f.read()
    return string.Template( templateText )

parser = OptionParser(usage="%prog [topfile used as template] [topfile2 ...]")

parser.add_option("-o", "--outfile", dest="OUTFILE",
                  default="merged.top", help="file for merged .top output.")

opts, args = parser.parse_args()

if len(args) < 2:
    sys.stderr.write("Specify in order: min, max\n")
    sys.exit(1)

prefix = '4Q_'

print '\n---------------------------------------------'
print 'collecting cross sections from ', prefix
print 'running from', args[0], 'to', args[1]
print '---------------------------------------------'


#file counter
files=0
xsection=0
error=0

#loop over the expected files and look for ##Number or ##T and :
for i in range(int(args[0]),int(args[1])):
    filename = prefix + str(i) + '.stat'
    if(os.path.isfile(filename) is False): continue
    files += 1
     # print 'file:', filename
    infile = open(filename,"r")
    for line in infile:
        if 'avgwgt(pb)=' in line and len(line.split()) is 4:
            print line.split()[1], line.split()[3]
            xsection += float(line.split()[1])
            error += float(line.split()[3])**2

print '---------------------------------------------'
print 'total number of files used:', files
print 'cross section average=', xsection/float(files), '+-', sqrt(error)/(float(files))
