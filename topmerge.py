#! /usr/bin/env python

import sys, os, copy
from math import sqrt
from subprocess import Popen
from optparse import OptionParser
#import lighthisto
import math

def getTemplate(basename):
    with open('%s.template' % basename, 'r') as f:
        templateText = f.read()
    return string.Template( templateText )

parser = OptionParser(usage="%prog [topfile used as template] [topfile2 ...]")

parser.add_option("-o", "--outfile", dest="OUTFILE",
                  default="merged.top", help="file for merged .top output.")

parser.add_option("-a", "--add", dest="addhistos",
                  default=False, help="add up the histograms", action="store_true")


parser.add_option("-s", "--sub", dest="subhistos",
                  default=False, help="subtract histograms", action="store_true")


opts, args = parser.parse_args()

if len(args) < 2:
    sys.stderr.write("At least two .top histogram files must be specified\n")
    sys.exit(1)

    #print opts.addhistos

templ_created = False
num_histo = 0
nh = 0

hist = {}
headerdata = {}
current_hdata = []
current_hist = []
current_name = ''
   
for topfile in args:

    if(templ_created is False):
        infile = open(topfile,"r")
        print 'Creating merge template from', topfile
        for line in infile:
            current_name = 'template.' + str(num_histo);
            if('HIST' in line):
                headerdata[current_name] = current_hdata
                num_histo += 1
                current_hdata = []
            if((line.split()[0]).isalpha() is True and 'HIST' not in line):
                current_hdata.append(line)             
        templ_created = True
        #current_name = 'template.' + '0'
        #hd_print = headerdata[current_name]
        #print hist_print
        #for i in hd_print:
        #    print i
        
    print "Processing file",topfile
    infile = open(topfile,"r")
    nh = 0
    for line in infile:
        if('HIST' in line):
            current_name = topfile.replace('top','') + str(nh);
            hist[current_name] = current_hist
            nh += 1 
            current_hist = []
        if((line.split()[0]).isalpha() is False):
            current_hist.append(' '.join((line.split()[0],(line.split()[1]))))
                
        
print 'Number of Histograms:', num_histo
#print hist.keys()
#print headerdata.keys()

#TOPMERGED = getTemplate(opts.OUTFILE)
print 'Writing to', opts.OUTFILE

try:
    outtop = open(opts.OUTFILE, "w")
except:
    sys.stderr.write("Couldn't open outfile %s for writing." % opts.OUTFILE)

template_name = ''

#current_name = 'template.' + '0'
#print current_name
#hd_print = headerdata[current_name]
#for i in hd_print:
    #    print i

namestr = []
fac = 1
if opts.subhistos is True:
    opts.addhistos = True
    
if opts.addhistos is True:
    summedhist = {}
    for ss in range(0,num_histo):
        summedhist[ss] = []
        #print summedhist
    cc = 0
    for k in range(num_histo):
        first_histo = True
        ntopfile = 1
        for topfile in args:
            cc += 1
            #print topfile
            hist_name = topfile.replace('top','') + str(k);
            #print hist_name
            if(first_histo is True):
                summedhist[k] = hist[hist_name]
                #print 'summedhist is...'
                #print summedhist[k]
                first_histo = False
            #namestr.append(hist_name)
            #print hist_name
            hist_write = hist[hist_name]
            for p in range(1,len(hist_write)):
                #print hist_write[p]
                fac = 1
                if ntopfile is 1:
                    fac = 0
                if opts.subhistos is True and ntopfile is 2:
                    fac = -1
                    #print 'fac = -1', hist_name
                if math.isnan(float(hist_write[p].split()[1])) is False:
                    summedhist[k][p] = summedhist[k][p].split()[0] + '\t' +  str(float(summedhist[k][p].split()[1]) + fac * float(hist_write[p].split()[1]))
                if k is 7:
                    print '\ta', summedhist[k][p].split()[1]
                #outtop.write(str(p) + '\n')
                # stringout = 'HIST \n'
                #+ colourstr[cc] + '\n'    
                #outtop.write(stringout)
                #  print 'HIST'
            ntopfile += 1
    for j in range(num_histo):
        cc = 0
        #print j
        template_name = 'template.' + str(j)
        # print template_name
        template_write = headerdata[template_name]
        for i in template_write:
            outtop.write(str(i))
            #     print i
        for p in summedhist[j]:
            #print p
            outtop.write(str(p) + '\n')
        stringout = 'HIST \n'
            #+ colourstr[cc] + '\n'    
        outtop.write(stringout)
            #  print 'HIST'
            #titleright = 'TITLE RIGHT\n '
        #for co in range(cc):
        #     titleright += colourstr[co] + ':' + namestr[co] + '   '
        #titleright += '\'\n'    
        #    print titleright
        #outtop.write(titleright)
    exit()


colourstr = [ '', 'BLUE', 'RED', 'GREEN', 'CYAN', 'DASHES', 'BLUE DASHES', 'RED DASHES', 'GREEN DASHES', 'CYAN DASHES', 'DOT DASHES', 'BLUE DOT DASHES', 'RED DOT DASHES', 'GREEN DOT DASHES', 'CYAN DOT DASHES' ]
for j in range(num_histo):
    cc = 0
    #print j
    template_name = 'template.' + str(j)
    # print template_name
    template_write = headerdata[template_name]
    for i in template_write:
        outtop.write(str(i))
        #     print i
    for topfile in args:
        hist_name = topfile.replace('top','') + str(j);
        namestr.append(hist_name)
        hist_write = hist[hist_name]
        for p in hist_write:
            outtop.write(str(p) + '\n')
        stringout = 'HIST ' + colourstr[cc] + '\n' 
        outtop.write(stringout)
	cc+=1   
    titleright = 'TITLE RIGHT \'BLACK'
    for co in range(cc):
        titleright += colourstr[co] + ':' + namestr[co] + '   '
    titleright += '\'\n'    
    #    print titleright
    outtop.write(titleright)
