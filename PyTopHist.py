import mpmath as mp
import numpy as np
import pylab as pl
from scipy import interpolate, signal
import matplotlib.font_manager as fm
from datetime import date
from matplotlib.ticker import MultipleLocator
import matplotlib.patches as mpatches
import math
from scipy.interpolate import interp1d
from collections import defaultdict
from collections import OrderedDict
import matplotlib.gridspec as gridspec


def hplot(ax, x, y, lsize, colour, lab, lstyle):
    halfdeltax_plus= []
    halfdeltax_minus= []
    xhigh = []
    yinfty = []
    xmv = []
    ymv = []
    for ii in range(0,len(x)):
        halfdeltax_plus.append(0.5*(x[1]-x[0]))
        if (x[ii] - 0.5*(x[1]-x[0])) > 0: 
            halfdeltax_minus.append(0.5*(x[1]-x[0]))
        else:
            halfdeltax_minus.append(x[ii]-0.01)
        xhigh.append(x[ii]+(x[1]-x[0])*0.5)
        yinfty.append(y[ii]-1E99)
        xmv.append(x[ii])
        if y[ii] > 1E-20:
            ymv.append(y[ii])
        else:
            ymv.append(1E-20)
    p1 = ax.errorbar(xmv, ymv, yerr=0.,xerr=[halfdeltax_minus, halfdeltax_plus], fmt='', elinewidth=lsize, ms=0, color=colour, ecolor=colour, capsize=0, label=None, lw=0)
    p2 = ax.plot(xmv, yinfty, color=colour,label=lab, lw=lsize, linestyle=lstyle)
    p1[-1][0].set_linestyle(lstyle)
    for iii in range(0,len(y)-1):
        lo1 = ax.vlines(xhigh[iii], ymv[iii+1], ymv[iii], color=colour, label=None, linestyle=lstyle, linewidth=lsize)

def hplot2(ax, x, y, lsize, colour, lab, lstyle, dasheslen, dashessp):
    halfdeltax_plus= []
    halfdeltax_minus= []
    xhigh = []
    yinfty = []
    xmv = []
    ymv = []
    for ii in range(0,len(x)):
        halfdeltax_plus.append(0.5*(x[1]-x[0]))
        if (x[ii] - 0.5*(x[1]-x[0])) > 0: 
            halfdeltax_minus.append(0.5*(x[1]-x[0]))
        else:
            halfdeltax_minus.append(x[ii]-0.01)
        if ii == len(x)-1:
            halfdeltax_plus[ii] = 0.25 * (x[1]-x[0])
        xhigh.append(x[ii]+(x[1]-x[0])*0.5)
        yinfty.append(y[ii]-1E99)
        xmv.append(x[ii])
        if y[ii] > 1E-20:
            ymv.append(y[ii])
        else:
            ymv.append(1E-20)
    p1 = ax.errorbar(xmv, ymv, yerr=0.,xerr=[halfdeltax_minus, halfdeltax_plus], fmt='', elinewidth=lsize, ms=0, color=colour, ecolor=colour, capsize=0, label=None, lw=0, dashes=(dasheslen, dashessp))
    p2 = ax.plot(xmv, yinfty, color=colour,label=lab, lw=lsize, linestyle=lstyle, dashes=(dasheslen, dashessp))
    p1[-1][0].set_linestyle(lstyle)
    for iii in range(0,len(y)-1):
        lo1 = ax.vlines(xhigh[iii], ymv[iii+1], ymv[iii], color=colour, label=None, linestyle=lstyle, linewidth=lsize)



def plot_top_xsec(yodafilename, plot_to_make, lab, colour, style, xsec):
    infile = open(yodafilename,"r")
    reading = False
    plot_found = False
    inputfile = yodafilename
    #sum = {}
    sum = defaultdict(lambda: 0)
    errsq = defaultdict(lambda: 0)
    print plot_to_make
    for line in infile:
       # print line
        if 'HIST' in line and reading is True:
            reading = False
            break
        if reading:
            #print line
            val = float(line.split()[1])
            xav = float(line.split()[0])
            #print 'xlo, xhi', xlo, xhi
            #print 'xav, yval, val', xav, yav, val
            sum[xav] = val
            #print xav, yav
        if 'SET LIMITS X' in line and plot_found is True:
            reading = True
        if 'TITLE TOP' in line and plot_to_make in line:
            plot_found = True
            print 'plot found:', plot_to_make
    x = []
    y = []
    ysum = 0
    sumord = OrderedDict(sorted(sum.items(), key=lambda t: t[0]))
    for s in sumord:
        #print s, sumord[s]
        x.append(s)
        y.append(sum[s])
        ysum = ysum + sum[s]
    # normalize:
    yn = []
    errn = []
    ii = 0
    for yi in y:
        yi = xsec*yi/ysum
        yn.append(yi)
        ii = ii + 1
    if plot_found is False:
        print 'WARNING, plot', plot_to_make, 'not found in', yodafilename, '!'
    pl.plot(x, yn,linewidth=2.0, color=colour, label=lab, ls=style, marker='')


def hist_top_xsec_ratio(yodafilename, plot_to_make, lab, colour, style, yodafilenameratio, xlabel, xsec1, xsec2, fig, gs):
    yr = hist_top_xsec_data(yodafilenameratio, plot_to_make, lab, colour, style, xsec2)
    infile = open(yodafilename,"r")
    reading = False
    plot_found = False
    inputfile = yodafilename
    #sum = {}
    sum = defaultdict(lambda: 0)
    errsq = defaultdict(lambda: 0)
    print plot_to_make
    plot_to_make = plot_to_make
    for line in infile:
       # print line
        if 'HIST' in line and reading is True:
            reading = False
            break
        if reading:
            val = float(line.split()[1])
            xav = float(line.split()[0])
            #print 'xlo, xhi', xlo, xhi
            #print 'xav, yval, val', xav, yav, val
            sum[xav] = val
            #print xav, yav
            #print xav, yav
        if 'SET LIMITS X' in line and plot_found is True:
            reading = True
        if 'TITLE TOP' in line and plot_to_make in line:
            plot_found = True
            print 'plot found:', plot_to_make

    sumord = OrderedDict(sorted(sum.items(), key=lambda t: t[0]))
    #print sumord
    x = []
    y = []
    e = []
    ysum = 0
    errysumsq = 0
    for s in sumord:
        #print s, sumord[s]
        x.append(s)
        y.append(sum[s])
        e.append(math.sqrt(abs(errsq[s])))
        #print s, sum[s],math.sqrt(errsq[s])
        ysum = ysum + sum[s]
        errysumsq = errysumsq + errsq[s]
    # normalize:
    errysum = math.sqrt(errysumsq)
    yn = []
    errn = []
    ii = 0
    for yi in y:
        yi = xsec1*yi/ysum
        #print 'error on y', e[ii]
        if e[ii] != 0:
            erryi = yi * math.sqrt( errysum**2 / ysum**2 + e[ii]**2 / y[ii]**2)
        else:
            erryi = 0.
        #print erryi
        if yr[ii] != 0.:
            yn.append(yi/yr[ii])
        if yr[ii] == 0.:
            yn.append(0.)
        ii = ii + 1
    ysum_norm = 0
    #for yi in yn:
    #    ysum_norm = ysum_norm + yi
    #print "normalization test", ysum_norm
    if plot_found is False:
        print 'WARNING, plot', plot_to_make, 'not found in', yodafilename, '!'
    ax2 = fig.add_subplot(gs[3,:])
    ax2.set_xlabel(xlabel,fontsize=20)
    ax2.plot(x, yn,linewidth=2.0, color=colour, label=lab, ls=style, marker='')
    ax2.legend()
    ax2.legend(loc="best", numpoints=1, frameon=False, prop={'size':8})
    return ax2
    #pl.errorbar(x, yn, yerr=errn, fmt='', color=colour, ls=style, label=lab)

def hist_top_xsec_ratio_stacked(plot_to_make, lab, colour, style, xlabel, yodafilenames, yodafilenames2, xsecs):
    # get x-values from the first filename: these should all be the same
    x = []
    x, xup, xdown, dx, yn_n = (yodafilenames, plot_to_make, lab, colour, style, xsecs)
    x, xup, xdown, dx, yn_d = hist_top_xsec_data_stack(yodafilenames2, plot_to_make, lab, colour, style, xsecs)
    yn = []
    yj = 0
    for j in range(0,len(yn_n)):
        if yn_d[j] != 0.:
            yj = yn_n[j]/yn_d[j]
        yn.append(yj)
    print yn
    ax2 = fig.add_subplot(gs[3,:])
    ax2.set_xlabel(xlabel,fontsize=20)
    ax2.plot(x, yn,linewidth=2.0, color=colour, label=lab, ls=style, marker='')
    ax2.legend()
    ax2.legend(loc="best", numpoints=1, frameon=False, prop={'size':8})
    return ax2
        
def hist_top_xsec_stacked(plot_to_make, lab, colour, style, yodafilenames, xsecs):
    # get x-values from the first filename: these should all be the same
      x = []
      x, xup, xdown, dx, yn = hist_top_xsec_data_stack(yodafilenames, plot_to_make, lab, colour, style, xsecs)
      for b in range(0,len(yn)):
        if b < len(yn)-1:
            ax.vlines(xup[b], yn[b+1], yn[b], color=colour)
            #print histogram.x[b],  histogram.val[b]/DataHisto.val[b], xpos
            ax.errorbar(x[b], yn[b], yerr=0., xerr = dx[b], fmt='', elinewidth='1', ms=0, color=colour, capsize=0, label="")
      pl.plot(x[0], yn[0],linewidth=2.0, color=colour, label=lab, ls='-', marker='')

def hist_top_stacked(plot_to_make, lab, colour, style, yodafilenames, xsecs, norm):
    # get x-values from the first filename: these should all be the same
      x = []
      x, xup, xdown, dx, yn = hist_top_data_stack(yodafilenames, plot_to_make, lab, colour, style, xsecs, norm)
      for b in range(0,len(yn)):
        if b < len(yn)-1:
            ax.vlines(xup[b], yn[b+1], yn[b], color=colour)
            #print histogram.x[b],  histogram.val[b]/DataHisto.val[b], xpos
            ax.errorbar(x[b], yn[b], yerr=0., xerr = dx[b], fmt='', elinewidth='1', ms=0, color=colour, capsize=0, label="")
      pl.plot(x[0], yn[0],linewidth=2.0, color=colour, label=lab, ls='-', marker='')   
    
def hist_top_xsec(yodafilename, plot_to_make, lab, colour, style, xsec):
    infile = open(yodafilename,"r")
    reading = False
    plot_found = False
    inputfile = yodafilename
    #sum = {}
    sum = defaultdict(lambda: 0)
    xlo_d = defaultdict(lambda: 0)
    xhi_d = defaultdict(lambda: 0)
    deltax_d = defaultdict(lambda: 0)
    errsq = defaultdict(lambda: 0)
    print plot_to_make
    plot_to_make = plot_to_make
    xav = -9999
    dxi = -9999
    for line in infile:
        #print line
        if 'HIST' in line and reading is True:
            reading = False
            break
        if reading:
            #print line
            val = float(line.split()[1])
            xav_old = xav
            xav = float(line.split()[0])
            if xav_old is not -9999:
                dxi = 0.5*(xav-xav_old)
               # print dxi
            sum[xav] = val
            #print xav, yav
            #print xav, yav
        if 'SET LIMITS X' in line and plot_found is True:
            reading = True
        if 'TITLE TOP' in line and plot_to_make in line:
            plot_found = True
            print 'plot found:', plot_to_make
    x = []
    y = []
    xup = []
    xdown = []
    dx = []
    ysum = 0
    sumord = OrderedDict(sorted(sum.items(), key=lambda t: t[0]))
    for s in sumord:
        #print s, sumord[s]
        x.append(s)
        xup.append(s+dxi)
        xdown.append(s-dxi)
        dx.append(dxi)
        y.append(sum[s])
        ysum = ysum + sum[s]
    # normalize:
    yn = []
    errn = []
    ii = 0
    for yi in y:
        yi = xsec*yi/ysum
        yn.append(yi)
        ii = ii + 1
    if plot_found is False:
        print 'WARNING, plot', plot_to_make, 'not found in', yodafilename, '!'
    for b in range(0,len(yn)):
        if b < len(yn)-1:
            ax.vlines(xup[b], yn[b+1], yn[b], color=colour)
            #print histogram.x[b],  histogram.val[b]/DataHisto.val[b], xpos
            ax.errorbar(x[b], yn[b], yerr=0., xerr = dx[b], fmt='', elinewidth='1', ms=0, color=colour, capsize=0, label="")
    pl.plot(x[0], yn[0],linewidth=2.0, color=colour, label=lab, ls='-', marker='')


def hist_top_getxvalues(yodafilename, plot_to_make):
    #set these to nothing as they are not used:
    lab = ''
    colour = ''
    style = ''
    xsec  = 1.
    infile = open(yodafilename,"r")
    reading = False
    plot_found = False
    inputfile = yodafilename
    #sum = {}
    sum = defaultdict(lambda: 0)
    xlo_d = defaultdict(lambda: 0)
    xhi_d = defaultdict(lambda: 0)
    deltax_d = defaultdict(lambda: 0)
    errsq = defaultdict(lambda: 0)
    print plot_to_make
    plot_to_make = plot_to_make
    xav = -9999
    dxi = -9999
    for line in infile:
        #print line
        if 'HIST' in line and reading is True:
            reading = False
            break
        if reading:
            #print line
            val = float(line.split()[1])
            xav_old = xav
            xav = float(line.split()[0])
            if xav_old is not -9999:
                dxi = 0.5*(xav-xav_old)
               # print dxi
            sum[xav] = val
            #print xav, yav
            #print xav, yav
        if 'SET LIMITS X' in line and plot_found is True:
            reading = True
        if 'TITLE TOP' in line and plot_to_make in line:
            plot_found = True
            print 'plot found:', plot_to_make
    x = []
    y = []
    xup = []
    xdown = []
    dx = []
    ysum = 0
    sumord = OrderedDict(sorted(sum.items(), key=lambda t: t[0]))
    for s in sumord:
        #print s, sumord[s]
        x.append(s)
        xup.append(s+dxi)
        xdown.append(s-dxi)
        dx.append(dxi)
        y.append(sum[s])
        ysum = ysum + sum[s]
    # normalize:
    yn = []
    errn = []
    ii = 0
    for yi in y:
        yi = xsec*yi/ysum
        yn.append(yi)
        ii = ii + 1
    if plot_found is False:
        print 'WARNING, plot', plot_to_make, 'not found in', yodafilename, '!'
    return x, xup, xdown, dx

    
def hist_top_xsec_data_stack(yodafilenames, plot_to_make, lab, colour, style, xsecs):
    yn_all = {}
    yn_stacked = []
    for yodafilename in yodafilenames:
        xsec = xsecs[yodafilename]
        infile = open(yodafilename,"r")
        reading = False
        plot_found = False
        inputfile = yodafilename
        #sum = {}
        sum = defaultdict(lambda: 0)
        xlo_d = defaultdict(lambda: 0)
        xhi_d = defaultdict(lambda: 0)
        deltax_d = defaultdict(lambda: 0)
        errsq = defaultdict(lambda: 0)
        print plot_to_make
        plot_to_make = plot_to_make
        xav = -9999
        dxi = -9999
        for line in infile:
            #print line
            if 'HIST' in line and reading is True:
                reading = False
                break
            if reading:
                #print line
                val = float(line.split()[1])
                xav_old = xav
                xav = float(line.split()[0])
                if xav_old is not -9999:
                    dxi = 0.5*(xav-xav_old)
               # print dxi
                sum[xav] = val
                #print xav, yav
                #print xav, yav
            if 'SET LIMITS X' in line and plot_found is True:
                reading = True
            if 'TITLE TOP' in line and plot_to_make in line:
                plot_found = True
                print 'plot found:', plot_to_make
        yn = []
        x = []
        y = []
        xup = []
        xdown = []
        dx = []
        ysum = 0
        sumord = OrderedDict(sorted(sum.items(), key=lambda t: t[0]))
        for s in sumord:
            #print s, sumord[s]
            x.append(s)
            xup.append(s+dxi)
            xdown.append(s-dxi)
            dx.append(dxi)
            y.append(sum[s])
            ysum = ysum + sum[s]
        # normalize:
        errn = []
        ii = 0
        for yi in y:
            yi = xsec*yi/ysum
            yn.append(yi)
            ii = ii + 1
        if plot_found is False:
            print 'WARNING, plot', plot_to_make, 'not found in', yodafilename, '!'
        yn_all[yodafilename] = yn
        not_resized = True
    for key in yn_all:
        jj = 0
        for yjj in yn_all[key]:
            if not_resized:
                for ll in range(0,len(yn_all[key])):
                    yn_stacked.append(0.)
                not_resized = False
            yn_stacked[jj] = yn_stacked[jj] + yn_all[key][jj]
            jj = jj + 1
    return x, xup, xdown, dx, yn_stacked

def hist_top_xsec_data(yodafilename, plot_to_make, lab, colour, style, xsec):
    infile = open(yodafilename,"r")
    reading = False
    plot_found = False
    inputfile = yodafilename
    #sum = {}
    sum = defaultdict(lambda: 0)
    xlo_d = defaultdict(lambda: 0)
    xhi_d = defaultdict(lambda: 0)
    deltax_d = defaultdict(lambda: 0)
    errsq = defaultdict(lambda: 0)
    print plot_to_make
    plot_to_make = plot_to_make
    xav = -9999
    dxi = -9999
    for line in infile:
        #print line
        if 'HIST' in line and reading is True:
            reading = False
            break
        if reading:
            #print line
            val = float(line.split()[1])
            xav_old = xav
            xav = float(line.split()[0])
            if xav_old is not -9999:
                dxi = 0.5*(xav-xav_old)
               # print dxi
            sum[xav] = val
            #print xav, yav
            #print xav, yav
        if 'SET LIMITS X' in line and plot_found is True:
            reading = True
        if 'TITLE TOP' in line and plot_to_make in line:
            plot_found = True
            print 'plot found:', plot_to_make
    x = []
    y = []
    xup = []
    xdown = []
    dx = []
    ysum = 0
    sumord = OrderedDict(sorted(sum.items(), key=lambda t: t[0]))
    for s in sumord:
        #print s, sumord[s]
        x.append(s)
        xup.append(s+dxi)
        xdown.append(s-dxi)
        dx.append(dxi)
        y.append(sum[s])
        ysum = ysum + sum[s]
    # normalize:
    yn = []
    errn = []
    ii = 0
    for yi in y:
        yi = xsec*yi/ysum
        yn.append(yi)
        ii = ii + 1
    if plot_found is False:
        print 'WARNING, plot', plot_to_make, 'not found in', yodafilename, '!'
    return yn
    #for b in range(0,len(yn)):
    #    if b < len(yn)-1:
    #        ax.vlines(xup[b], yn[b+1], yn[b], color=colour)
    #        #print histogram.x[b],  histogram.val[b]/DataHisto.val[b], xpos
    #        ax.errorbar(x[b], yn[b], yerr=0., xerr = dx[b], fmt='', elinewidth='1', ms=0, color=colour, capsize=0, label="")
    #pl.plot(x[0], yn[0],linewidth=2.0, color=colour, label=lab, ls='-', marker='')

def hist_top_data_stack(yodafilenames, plot_to_make, lab, colour, style):
    yn_all = {}
    yn_stacked = []
    for yodafilename in yodafilenames:
        infile = open(yodafilename,"r")
        reading = False
        plot_found = False
        inputfile = yodafilename
        #sum = {}
        sum = defaultdict(lambda: 0)
        xlo_d = defaultdict(lambda: 0)
        xhi_d = defaultdict(lambda: 0)
        deltax_d = defaultdict(lambda: 0)
        errsq = defaultdict(lambda: 0)
        print plot_to_make
        plot_to_make = plot_to_make
        xav = -9999
        dxi = -9999
        for line in infile:
            #print line
            if 'HIST' in line and reading is True:
                reading = False
                break
            if reading:
                #print line
                val = float(line.split()[1])
                xav_old = xav
                xav = float(line.split()[0])
                if xav_old is not -9999:
                    dxi = 0.5*(xav-xav_old)
               # print dxi
                sum[xav] = val
                #print xav, yav
                #print xav, yav
            if 'SET LIMITS X' in line and plot_found is True:
                reading = True
            if 'TITLE TOP' in line and plot_to_make in line:
                plot_found = True
                print 'plot found:', plot_to_make
        yn = []
        x = []
        y = []
        xup = []
        xdown = []
        dx = []
        ysum = 0
        sumord = OrderedDict(sorted(sum.items(), key=lambda t: t[0]))
        for s in sumord:
            #print s, sumord[s]
            x.append(s)
            xup.append(s+dxi)
            xdown.append(s-dxi)
            dx.append(dxi)
            y.append(sum[s])
            ysum = ysum + sum[s]
        # normalize:
        errn = []
        ii = 0
        for yi in y:
            #yi = xsec*yi/ysum
            yn.append(yi)
            ii = ii + 1
        if plot_found is False:
            print 'WARNING, plot', plot_to_make, 'not found in', yodafilename, '!'
        yn_all[yodafilename] = yn
        not_resized = True
    for key in yn_all:
        jj = 0
        for yjj in yn_all[key]:
            if not_resized:
                for ll in range(0,len(yn_all[key])):
                    yn_stacked.append(0.)
                not_resized = False
            yn_stacked[jj] = yn_stacked[jj] + yn_all[key][jj]
            jj = jj + 1
    return x, xup, xdown, dx, yn_stacked


def plot_top(yodafilename, plot_to_make, lab, colour, style):
   plot_top_xsec(yodafilename, plot_to_make, lab, colour, style, 1.0)


def hist_top(yodafilename, plot_to_make, lab, colour, style, xsec, norm, ax):
    infile = open(yodafilename,"r")
    reading = False
    plot_found = False
    inputfile = yodafilename
    #sum = {}
    sum = defaultdict(lambda: 0)
    xlo_d = defaultdict(lambda: 0)
    xhi_d = defaultdict(lambda: 0)
    deltax_d = defaultdict(lambda: 0)
    errsq = defaultdict(lambda: 0)
    print plot_to_make
    plot_to_make = plot_to_make
    xav = -9999
    dxi = -9999
    for line in infile:
        #print line
        if 'HIST' in line and reading is True:
            reading = False
            break
        if reading:
            #print line
            val = float(line.split()[1])
            xav_old = xav
            xav = float(line.split()[0])
            if xav_old is not -9999:
                dxi = 0.5*(xav-xav_old)
               # print dxi
            sum[xav] = val
            #print xav, yav
            #print xav, yav
        if 'SET LIMITS X' in line and plot_found is True:
            reading = True
        if 'TITLE TOP' in line and plot_to_make in line:
            plot_found = True
            print 'plot found:', plot_to_make
    x = []
    y = []
    xup = []
    xdown = []
    dx = []
    ysum = 0
    sumord = OrderedDict(sorted(sum.items(), key=lambda t: t[0]))
    for s in sumord:
        #print s, sumord[s]
        x.append(s)
        xup.append(s+dxi)
        xdown.append(s-dxi)
        dx.append(dxi)
        y.append(sum[s])
        ysum = ysum + sum[s]
    # normalize:
    yn = []
    errn = []
    ii = 0
    for yi in y:
        if norm:
            yi = xsec*yi/ysum
        yn.append(yi)
        ii = ii + 1
    if plot_found is False:
        print 'WARNING, plot', plot_to_make, 'not found in', yodafilename, '!'
    for b in range(0,len(yn)):
        if b < len(yn)-1:
            ax.vlines(xup[b], yn[b+1], yn[b], color=colour)
            #print histogram.x[b],  histogram.val[b]/DataHisto.val[b], xpos
            ax.errorbar(x[b], yn[b], yerr=0., xerr = dx[b], fmt='', elinewidth='1', ms=0, color=colour, capsize=0, label="")
    pl.plot(x[0], yn[0],linewidth=2.0, color=colour, label=lab, ls='-', marker='')


def hist_top_envelope(plot_to_make, lab, colour, style, yodafilename, xsec, ax):
    # get x-values from the first filename: these should all be the same
    yodafilenames = []
    yodafilenames.append(yodafilename)
    x = []
    # pdfs
    x, xup, xdown, dx, yn_pu = hist_top_data_stack(yodafilenames, plot_to_make + ' (pdf, up)', lab, colour, style)
    x, xup, xdown, dx, yn_pd = hist_top_data_stack(yodafilenames, plot_to_make + ' (pdf, down)', lab, colour, style)
    x, xup, xdown, dx, yn_pc = hist_top_data_stack(yodafilenames, plot_to_make + ' (pdf, central)', lab, colour, style)
    # scales
    x, xup, xdown, dx, yn_su = hist_top_data_stack(yodafilenames, plot_to_make + ' (scale, up)', lab, colour, style)
    x, xup, xdown, dx, yn_sd = hist_top_data_stack(yodafilenames, plot_to_make + ' (scale, down)', lab, colour, style)
    x, xup, xdown, dx, yn_sc = hist_top_data_stack(yodafilenames, plot_to_make + ' (scale, central)', lab, colour, style)
    
    yn_u = []
    yn_d = []
    yn_c = []
    yj = 0
    ysum_c = 0
    ysum_u = 0
    ysum_d = 0
    for k in range(0,len(yn_pc)):
        ysum_c = ysum_c + yn_pc[k]       
    for j in range(0,len(yn_pu)):
        DeltaF = yn_pu[j] - yn_pc[j]
        yi_u = yn_pc[j] + math.sqrt( DeltaF**2 + (yn_su[j]-yn_sc[j])**2 )
        yi_d = yn_pc[j] - math.sqrt( DeltaF**2 + (yn_sc[j]-yn_sd[j])**2 )
        yn_u.append(yi_u)
        yn_d.append(yi_d)
        ysum_u = ysum_u + yi_u
        ysum_d = ysum_d + yi_d
        yn_c.append(yn_pc[j])
    yn_u_norm = []
    yn_d_norm = []
    yn_c_norm = []
    xsec_c = xsec
    xsec_u = xsec * ysum_u / ysum_c
    xsec_d = xsec * ysum_d / ysum_c
    for l in range(0,len(yn_pu)):
        yi_u_norm = yn_u[l] * xsec_u / ysum_u
        yi_d_norm = yn_d[l] * xsec_d / ysum_d
        yi_c_norm = yn_c[l] * xsec / ysum_c
        yn_u_norm.append(yi_u_norm)
        yn_d_norm.append(yi_d_norm)
        yn_c_norm.append(yi_c_norm)        
#    ax2 = fig.add_subplot(gs[3,:])
#    ax2.set_xlabel(xlabel,fontsize=20)
    #pl.plot(x, yn_u,linewidth=2.0, color=colour, label=lab, ls=style, marker='')
    #pl.plot(x, yn_d,linewidth=2.0, color=colour, label=lab, ls=style, marker='')
    pl.plot(x, yn_c_norm,linewidth=0.5, color=colour, label=lab, ls=style, marker='')
    ax.fill_between(x, yn_d_norm, yn_u_norm, facecolor=colour, interpolate=True, alpha=0.5)
    return 0
#   ax2.legend()
#    ax2.legend(loc="best", numpoints=1, frameon=False, prop={'size':8})
#    return ax2

def hist_top_pdf_ratio_to_central(plot_to_make, lab, colour, style, yodafilename, xsec, xlabel, fig, gs, alphaval, lines_or_band, nn_min, nn_max, setlabels, ax2):
    if setlabels:
        ax2.set_xlabel(xlabel,fontsize=20)
        ax2.legend()
        ax2.legend(loc="best", numpoints=1, frameon=False, prop={'size':8})
    # get x-values from the first filename: these should all be the same
    yodafilenames = []
    yodafilenames.append(yodafilename)
    x = []
    # pdfs
    yn = {}
    nreps = 0
    for nn in range(nn_min,nn_max+1):
        x, xup, xdown, dx, yi = hist_top_data_stack(yodafilenames, plot_to_make + ' PDF ' + str(nn), lab, colour, style)
        yn[nn] = yi
        nreps = nreps + 1

    ysum = []
    ysum_sq = []
    yav = []
    stddev = []
    for k in range(0,len(yn[0])):
        ysum.append(0)
        ysum_sq.append(0)
        yav.append(0)
        stddev.append(0)
        for nn in range(nn_min,nn_max+1):
            ysum[k] = ysum[k] + yn[nn][k] # the sum of values
            ysum_sq[k] = ysum_sq[k] + yn[nn][k]**2 # the sum of values
    for k in range(0,len(yn[0])):
        yav[k] = ysum[k]/nreps
        stddev[k] = math.sqrt(ysum_sq[k]/nreps - yav[k]**2)
    yn_u = []
    yn_d = []
    for j in range(0,len(yn[0])):
        yn_d.append(0)
        yn_u.append(0)
        if yav[j] != 0.:
            yn_u[j] = ((yav[j]+stddev[j])/yav[j])
            yn_d[j] = ((yav[j]-stddev[j])/yav[j])
    one = []
    for i in range(0,len(yn_d)):
        yn_d[i] = yn_d[i] - 1.0
        yn_u[i] = yn_u[i] - 1.0
    if lines_or_band == 'band':
       ax2.fill_between(x, yn_d, yn_u, facecolor=colour, interpolate=True, alpha=alphaval)
    if lines_or_band == 'lines':
        ax2.plot(x, yn_u,linewidth=1.0, color=colour, label=lab, ls=style, marker='')
        ax2.plot(x, yn_d,linewidth=1.0, color=colour, label='', ls=style, marker='')
    return ax2
#   ax2.legend()
#    ax2.legend(loc="best", numpoints=1, frameon=False, prop={'size':8})

def hist_top_envelope_ratio_to_central(plot_to_make, lab, colour, style, yodafilename, xsec, xlabel, fig, gs, alphaval, lines_or_band, setlabels):
    ax2 = fig.add_subplot(gs[2,:])
    if setlabels:
        ax2.set_xlabel(xlabel,fontsize=20)
        ax2.legend()
        ax2.legend(loc="best", numpoints=1, frameon=False, prop={'size':8})
    # get x-values from the first filename: these should all be the same
    yodafilenames = []
    yodafilenames.append(yodafilename)
    x = []
    # pdfs
    x, xup, xdown, dx, yn_pu = hist_top_data_stack(yodafilenames, plot_to_make + ' (pdf, up)', lab, colour, style)
    x, xup, xdown, dx, yn_pd = hist_top_data_stack(yodafilenames, plot_to_make + ' (pdf, down)', lab, colour, style)
    x, xup, xdown, dx, yn_pc = hist_top_data_stack(yodafilenames, plot_to_make + ' (pdf, central)', lab, colour, style)
    # scales
    x, xup, xdown, dx, yn_su = hist_top_data_stack(yodafilenames, plot_to_make + ' (scale, up)', lab, colour, style)
    x, xup, xdown, dx, yn_sd = hist_top_data_stack(yodafilenames, plot_to_make + ' (scale, down)', lab, colour, style)
    x, xup, xdown, dx, yn_sc = hist_top_data_stack(yodafilenames, plot_to_make + ' (scale, central)', lab, colour, style)
    
    yn_u = []
    yn_d = []
    yn_c = []
    yj = 0
    ysum_c = 0
    ysum_u = 0
    ysum_d = 0
    for k in range(0,len(yn_pc)):
        ysum_c = ysum_c + yn_pc[k]       
    for j in range(0,len(yn_pu)):
        DeltaF = yn_pu[j] - yn_pc[j]
        yi_u = yn_pc[j] + math.sqrt( DeltaF**2 + (yn_su[j]-yn_sc[j])**2 )
        yi_d = yn_pc[j] - math.sqrt( DeltaF**2 + (yn_sc[j]-yn_sd[j])**2 )
        yn_u.append(yi_u)
        yn_d.append(yi_d)
        ysum_u = ysum_u + yi_u
        ysum_d = ysum_d + yi_d
        yn_c.append(yn_pc[j])
    yn_u_norm = []
    yn_d_norm = []
    yn_c_norm = []
    yn_u_norm_r = []
    yn_d_norm_r = []
    yn_c_norm_r= []
    xsec_c = xsec
    xsec_u = xsec * ysum_u / ysum_c
    xsec_d = xsec * ysum_d / ysum_c
    for l in range(0,len(yn_pu)):
        yi_u_norm = yn_u[l] * xsec_u / ysum_u
        yi_d_norm = yn_d[l] * xsec_d / ysum_d
        yi_c_norm = yn_c[l] * xsec / ysum_c
        yn_u_norm.append(yi_u_norm)
        yn_d_norm.append(yi_d_norm)
        yn_c_norm.append(yi_c_norm)
        if yi_c_norm != 0:
            yn_u_norm_r.append(yi_u_norm/yi_c_norm)
            yn_d_norm_r.append(yi_d_norm/yi_c_norm)
        else:
            yn_u_norm_r.append(0.)
            yn_d_norm_r.append(0.)
    if lines_or_band == 'band':
       ax2.fill_between(x, yn_d_norm_r, yn_u_norm_r, facecolor=colour, interpolate=True, alpha=alphaval)
    if lines_or_band == 'lines':
        ax2.plot(x, yn_u_norm_r,linewidth=1.0, color=colour, label=lab, ls=style, marker='')
        ax2.plot(x, yn_d_norm_r,linewidth=1.0, color=colour, label='', ls=style, marker='')
    return ax2
#   ax2.legend()
#    ax2.legend(loc="best", numpoints=1, frameon=False, prop={'size':8})

def hist_top_scale_ratio_to_central(plot_to_make, lab, colour, style, yodafilename, xsec, xlabel, fig, gs, alphaval, lines_or_band, setlabels, ax2):
#    ax2 = fig.add_subplot(gs[2,:])
    if setlabels:
        ax2.set_xlabel(xlabel,fontsize=20)
        ax2.legend()
        ax2.legend(loc="best", numpoints=1, frameon=False, prop={'size':8})
    # get x-values from the first filename: these should all be the same
    yodafilenames = []
    yodafilenames.append(yodafilename)
    x = []
  
    # scales
    x, xup, xdown, dx, yn_su = hist_top_data_stack(yodafilenames, plot_to_make + ' (scale, up)', lab, colour, style)
    x, xup, xdown, dx, yn_sd = hist_top_data_stack(yodafilenames, plot_to_make + ' (scale, down)', lab, colour, style)
    x, xup, xdown, dx, yn_sc = hist_top_data_stack(yodafilenames, plot_to_make + ' (scale, central)', lab, colour, style)
    
    yn_u = []
    yn_d = []
    yn_c = []
    yj = 0
    ysum_c = 0
    ysum_u = 0
    ysum_d = 0
    for k in range(0,len(yn_sc)):
        ysum_c = ysum_c + yn_sc[k]       
    for j in range(0,len(yn_su)):
        DeltaF = 0
        yi_u = yn_sc[j] + math.sqrt((yn_su[j]-yn_sc[j])**2 )
        yi_d = yn_sc[j] - math.sqrt((yn_sc[j]-yn_sd[j])**2 )
        yn_u.append(yi_u)
        yn_d.append(yi_d)
        ysum_u = ysum_u + yi_u
        ysum_d = ysum_d + yi_d
        yn_c.append(yn_sc[j])
    yn_u_norm = []
    yn_d_norm = []
    yn_c_norm = []
    yn_u_norm_r = []
    yn_d_norm_r = []
    yn_c_norm_r= []
    xsec_c = xsec
    xsec_u = xsec * ysum_u / ysum_c
    xsec_d = xsec * ysum_d / ysum_c
    for l in range(0,len(yn_su)):
        yi_u_norm = yn_u[l] * xsec_u / ysum_u
        yi_d_norm = yn_d[l] * xsec_d / ysum_d
        yi_c_norm = yn_c[l] * xsec / ysum_c
        yn_u_norm.append(yi_u_norm)
        yn_d_norm.append(yi_d_norm)
        yn_c_norm.append(yi_c_norm)
        if yi_c_norm != 0:
            yn_u_norm_r.append(yi_u_norm/yi_c_norm)
            yn_d_norm_r.append(yi_d_norm/yi_c_norm)
        else:
            yn_u_norm_r.append(0.)
            yn_d_norm_r.append(0.)
    for i in range(0,len(yn_d_norm_r)):
        yn_d_norm_r[i] = yn_d_norm_r[i] - 1.0
        yn_u_norm_r[i] = yn_u_norm_r[i] - 1.0
    if lines_or_band == 'band':
       ax2.fill_between(x, yn_d_norm_r, yn_u_norm_r, facecolor=colour, interpolate=True, alpha=alphaval)
    if lines_or_band == 'lines':
        ax2.plot(x, yn_u_norm_r,linewidth=1.0, color=colour, label=lab, ls=style, marker='')
        ax2.plot(x, yn_d_norm_r,linewidth=1.0, color=colour, label='', ls=style, marker='')
    return ax2
#   ax2.legend()
#    ax2.legend(loc="best", numpoints=1, frameon=False, prop={'size':8})

def hist_top_both_ratio_to_central(plot_to_make, lab, colour, style, yodafilename, xsec, xlabel, fig, gs, alphaval, lines_or_band,nn_min,nn_max, setlabels, ax2):
#    ax2 = fig.add_subplot(gs[2,:])
    if setlabels:
        ax2.set_xlabel(xlabel,fontsize=20)
        ax2.legend()
        ax2.legend(loc="best", numpoints=1, frameon=False, prop={'size':8})
    # get x-values from the first filename: these should all be the same
    yodafilenames = []
    yodafilenames.append(yodafilename)
    x = []
    # pdfs
    yn = {}
    nreps = 0
    for nn in range(nn_min,nn_max+1):
        x, xup, xdown, dx, yi = hist_top_data_stack(yodafilenames, plot_to_make + ' PDF ' + str(nn), lab, colour, style)
        yn[nn] = yi
        nreps = nreps + 1

    ysum = []
    ysum_sq = []
    yav = []
    stddev = []
    for k in range(0,len(yn[0])):
        ysum.append(0)
        ysum_sq.append(0)
        yav.append(0)
        stddev.append(0)
        for nn in range(nn_min,nn_max+1):
            ysum[k] = ysum[k] + yn[nn][k] # the sum of values
            ysum_sq[k] = ysum_sq[k] + yn[nn][k]**2 # the sum of values
    for k in range(0,len(yn[0])):
        yav[k] = ysum[k]/nreps
        stddev[k] = math.sqrt(ysum_sq[k]/nreps - yav[k]**2)

    
    # scales
    x, xup, xdown, dx, yn_su = hist_top_data_stack(yodafilenames, plot_to_make + ' (scale, up)', lab, colour, style)
    x, xup, xdown, dx, yn_sd = hist_top_data_stack(yodafilenames, plot_to_make + ' (scale, down)', lab, colour, style)
    x, xup, xdown, dx, yn_sc = hist_top_data_stack(yodafilenames, plot_to_make + ' (scale, central)', lab, colour, style)
    
    yn_u = []
    yn_d = []
    yn_c = []
    yj = 0
    ysum_c = 0
    ysum_u = 0
    ysum_d = 0
    for k in range(0,len(yn_sc)):
        ysum_c = ysum_c + yn_sc[k]       
    for j in range(0,len(yn_su)):
        DeltaF = 0
        yi_u = yn_sc[j] + math.sqrt(stddev[j]**2 + (yn_su[j]-yn_sc[j])**2 )
        yi_d = yn_sc[j] - math.sqrt(stddev[j]**2 + (yn_sc[j]-yn_sd[j])**2 )
        yn_u.append(yi_u)
        yn_d.append(yi_d)
        ysum_u = ysum_u + yi_u
        ysum_d = ysum_d + yi_d
        yn_c.append(yn_sc[j])
    yn_u_norm = []
    yn_d_norm = []
    yn_c_norm = []
    yn_u_norm_r = []
    yn_d_norm_r = []
    yn_c_norm_r= []
    xsec_c = xsec
    xsec_u = xsec * ysum_u / ysum_c
    xsec_d = xsec * ysum_d / ysum_c
    for l in range(0,len(yn_su)):
        yi_u_norm = yn_u[l] * xsec_u / ysum_u
        yi_d_norm = yn_d[l] * xsec_d / ysum_d
        yi_c_norm = yn_c[l] * xsec / ysum_c
        yn_u_norm.append(yi_u_norm)
        yn_d_norm.append(yi_d_norm)
        yn_c_norm.append(yi_c_norm)
        if yi_c_norm != 0:
            yn_u_norm_r.append(yi_u_norm/yi_c_norm)
            yn_d_norm_r.append(yi_d_norm/yi_c_norm)
        else:
            yn_u_norm_r.append(0.)
            yn_d_norm_r.append(0.)
    if lines_or_band == 'band':
       ax2.fill_between(x, yn_d_norm_r, yn_u_norm_r, facecolor=colour, interpolate=True, alpha=alphaval)
    if lines_or_band == 'lines':
        ax2.plot(x, yn_u_norm_r,linewidth=1.0, color=colour, label=lab, ls=style, marker='')
        ax2.plot(x, yn_d_norm_r,linewidth=1.0, color=colour, label='', ls=style, marker='')
    return ax2
#   ax2.legend()
#    ax2.legend(loc="best", numpoints=1, frameon=False, prop={'size':8})


def data_top_envelope(plot_to_make, lab, colour, style, yodafilename, xsec):
    # get x-values from the first filename: these should all be the same
    yodafilenames = []
    yodafilenames.append(yodafilename)
    x = []
    # pdfs
    x, xup, xdown, dx, yn_pu = hist_top_data_stack(yodafilenames, plot_to_make + ' (pdf, up)', lab, colour, style)
    x, xup, xdown, dx, yn_pd = hist_top_data_stack(yodafilenames, plot_to_make + ' (pdf, down)', lab, colour, style)
    x, xup, xdown, dx, yn_pc = hist_top_data_stack(yodafilenames, plot_to_make + ' (pdf, central)', lab, colour, style)
    # scales
    x, xup, xdown, dx, yn_su = hist_top_data_stack(yodafilenames, plot_to_make + ' (scale, up)', lab, colour, style)
    x, xup, xdown, dx, yn_sd = hist_top_data_stack(yodafilenames, plot_to_make + ' (scale, down)', lab, colour, style)
    x, xup, xdown, dx, yn_sc = hist_top_data_stack(yodafilenames, plot_to_make + ' (scale, central)', lab, colour, style)
    
    yn_u = []
    yn_d = []
    yn_c = []
    yj = 0
    ysum_c = 0
    ysum_u = 0
    ysum_d = 0
    for k in range(0,len(yn_pc)):
        ysum_c = ysum_c + yn_pc[k]       
    for j in range(0,len(yn_pu)):
        DeltaF = yn_pu[j] - yn_pc[j]
        yi_u = yn_pc[j] + math.sqrt( DeltaF**2 + (yn_su[j]-yn_sc[j])**2 )
        yi_d = yn_pc[j] - math.sqrt( DeltaF**2 + (yn_sc[j]-yn_sd[j])**2 )
        yn_u.append(yi_u)
        yn_d.append(yi_d)
        ysum_u = ysum_u + yi_u
        ysum_d = ysum_d + yi_d
        yn_c.append(yn_pc[j])
    yn_u_norm = []
    yn_d_norm = []
    yn_c_norm = []
    yn_u_norm_r = []
    yn_d_norm_r = []
    yn_c_norm_r= []
    xsec_c = xsec
    xsec_u = xsec * ysum_u / ysum_c
    xsec_d = xsec * ysum_d / ysum_c
    for l in range(0,len(yn_pu)):
        yi_u_norm = yn_u[l] * xsec_u / ysum_u
        yi_d_norm = yn_d[l] * xsec_d / ysum_d
        yi_c_norm = yn_c[l] * xsec / ysum_c
        yn_u_norm.append(yi_u_norm)
        yn_d_norm.append(yi_d_norm)
        yn_c_norm.append(yi_c_norm)
        if yi_c_norm != 0:
            yn_u_norm_r.append(yi_u_norm/yi_c_norm)
            yn_d_norm_r.append(yi_d_norm/yi_c_norm)
        else:
            yn_u_norm_r.append(0.)
            yn_d_norm_r.append(0.)
    return x, yn_u_norm_r, yn_d_norm_r

def hist_top_envelope_processes(plot_to_make, lab, colour, style, yodafilename_zh, yodafilename_wh, xsec_zh, xsec_wh, xlabel, fig, gs, alphaval, lines_or_band):
    ax2 = fig.add_subplot(gs[3,:])
    ax2.set_xlabel(xlabel,fontsize=20)
    ax2.legend()
    ax2.legend(loc="best", numpoints=1, frameon=False, prop={'size':8})
    # get x-values from the first filename: these should all be the same
    yn_u_norm_r_proc = []
    yn_d_norm_r_proc = []
    x, yn_u_norm_r_zh, yn_d_norm_r_zh = data_top_envelope(plot_to_make, lab, colour, style, yodafilename_zh, xsec_zh)
    x, yn_u_norm_r_wh, yn_d_norm_r_wh = data_top_envelope(plot_to_make, lab, colour, style, yodafilename_wh, xsec_wh)
    for l in range(0,len(yn_u_norm_r_wh)):
        if yn_u_norm_r_wh[l] != 0:
                yi_u_norm_r_proc = yn_u_norm_r_zh[l] / yn_u_norm_r_wh[l]
        else:
            yi_u_norm_r_proc = 0.
        if yn_d_norm_r_wh[l] != 0:
            yi_d_norm_r_proc = yn_d_norm_r_zh[l] / yn_d_norm_r_wh[l]
        else:
            yi_d_norm_r_proc = 0.
        yn_u_norm_r_proc.append(yi_u_norm_r_proc)
        yn_d_norm_r_proc.append(yi_d_norm_r_proc)
    print  yn_u_norm_r_proc, yn_d_norm_r_proc
    if lines_or_band == 'band':
       ax2.fill_between(x, yn_d_norm_r_proc, yn_u_norm_r_proc, facecolor=colour, interpolate=True, alpha=alphaval)
       ax2.plot([0,14000], [1.0,1.0] ,linewidth=2.0, color=colour, label=lab, ls='-', marker='', alpha=alphaval)

    if lines_or_band == 'lines':
        ax2.plot(x, yn_u_norm_r_proc,linewidth=2.0, color=colour, label=lab, ls=style, marker='')
        ax2.plot(x, yn_d_norm_r_proc,linewidth=2.0, color=colour, label='', ls=style, marker='')

def hist_top_envelope_processes_uncorrelated(plot_to_make, lab, colour, style, yodafilename_zh, yodafilename_wh, xsec_zh, xsec_wh, xlabel, fig, gs, alphaval, lines_or_band):
    ax2 = fig.add_subplot(gs[3,:])
    ax2.set_xlabel(xlabel,fontsize=20)
    ax2.legend()
    ax2.legend(loc="best", numpoints=1, frameon=False, prop={'size':8})
    # get x-values from the first filename: these should all be the same
    yn_u_norm_r_proc = []
    yn_d_norm_r_proc = []
    x, yn_u_norm_r_zh, yn_d_norm_r_zh = data_top_envelope(plot_to_make, lab, colour, style, yodafilename_zh, xsec_zh)
    x, yn_u_norm_r_wh, yn_d_norm_r_wh = data_top_envelope(plot_to_make, lab, colour, style, yodafilename_wh, xsec_wh)
    for l in range(0,len(yn_u_norm_r_wh)):
        if yn_u_norm_r_wh[l] != 0:
                yi_u_norm_r_proc = yn_d_norm_r_zh[l] / yn_u_norm_r_wh[l]
        else:
            yi_u_norm_r_proc = 0.
        if yn_d_norm_r_wh[l] != 0:
            yi_d_norm_r_proc = yn_u_norm_r_zh[l] / yn_d_norm_r_wh[l]
        else:
            yi_d_norm_r_proc = 0.
        yn_u_norm_r_proc.append(yi_u_norm_r_proc)
        yn_d_norm_r_proc.append(yi_d_norm_r_proc)
    print  yn_u_norm_r_proc, yn_d_norm_r_proc
    if lines_or_band == 'band':
       ax2.fill_between(x, yn_d_norm_r_proc, yn_u_norm_r_proc, facecolor=colour, interpolate=True, alpha=alphaval)
       ax2.plot([0,14000], [1.0,1.0] ,linewidth=2.0, color=colour, label=lab, ls='-', marker='', alpha=alphaval)

    if lines_or_band == 'lines':
        ax2.plot(x, yn_u_norm_r_proc,linewidth=1.0, color=colour, label=lab, ls=style, marker='')
        ax2.plot(x, yn_d_norm_r_proc,linewidth=1.0, color=colour, label='', ls=style, marker='')


def hist_top_scale_ratio_to_central_R(plot_to_make, lab, colour, style, yodafilename_zh, yodafilename_wh, xsec_zh, xsec_wh, xlabel, fig, gs, alphaval, lines_or_band, setlabels, ax3):
    if setlabels:
        ax3.set_xlabel(xlabel,fontsize=20)
        ax3.legend()
        ax3.legend(loc="best", numpoints=1, frameon=False, prop={'size':8})
    # get x-values from the first filename: these should all be the same
    yodafilenames_zh = []
    yodafilenames_zh.append(yodafilename_zh)
    yodafilenames_wh = []
    yodafilenames_wh.append(yodafilename_wh)
    x = []
  
    # scales
    x, xup, xdown, dx, yn_su_zh = hist_top_data_stack(yodafilenames_zh, plot_to_make + ' (scale, up)', lab, colour, style)
    x, xup, xdown, dx, yn_sd_zh = hist_top_data_stack(yodafilenames_zh, plot_to_make + ' (scale, down)', lab, colour, style)
    x, xup, xdown, dx, yn_sc_zh = hist_top_data_stack(yodafilenames_zh, plot_to_make + ' (scale, central)', lab, colour, style)

    x, xup, xdown, dx, yn_su_wh = hist_top_data_stack(yodafilenames_wh, plot_to_make + ' (scale, up)', lab, colour, style)
    x, xup, xdown, dx, yn_sd_wh = hist_top_data_stack(yodafilenames_wh, plot_to_make + ' (scale, down)', lab, colour, style)
    x, xup, xdown, dx, yn_sc_wh = hist_top_data_stack(yodafilenames_wh, plot_to_make + ' (scale, central)', lab, colour, style)

    yn_u = []
    yn_d = []
    yn_c = []
    yj = 0
    ysum_c = 0
    ysum_u = 0
    ysum_d = 0
    for k in range(0,len(yn_sc_zh)):
        if yn_sc_wh[k] != 0.:
            ysum_c = ysum_c + yn_sc_zh[k]/yn_sc_wh[k]       
    for j in range(0,len(yn_su_zh)):
        yi_u = 0
        yi_d = 0
        yi_c = 0
        if yn_sc_wh[j] != 0.:
            yi_u = yn_sc_zh[j]/yn_sc_wh[j] + math.sqrt((yn_su_zh[j]/yn_su_wh[j]-yn_sc_zh[j]/yn_sc_wh[j])**2 )
            yi_d = yn_sc_zh[j]/yn_sc_wh[j] - math.sqrt((yn_sc_zh[j]/yn_sc_wh[j]-yn_sd_zh[j]/yn_sd_wh[j])**2 )
            yi_c = yn_sc_zh[j]/yn_sc_wh[j]
        ysum_u = ysum_u + yi_u
        ysum_d = ysum_d + yi_d
        yn_u.append(yi_u)
        yn_d.append(yi_d)
        yn_c.append(yi_c)
    yn_u_norm = []
    yn_d_norm = []
    yn_c_norm = []
    yn_u_norm_r = []
    yn_d_norm_r = []
    yn_c_norm_r= []
    for l in range(0,len(yn_su_zh)):
        yi_u_norm = yn_u[l] / ysum_u
        yi_d_norm = yn_d[l] / ysum_d
        yi_c_norm = yn_c[l] / ysum_c
        yn_u_norm.append(yi_u_norm)
        yn_d_norm.append(yi_d_norm)
        yn_c_norm.append(yi_c_norm)
        if yi_c_norm != 0:
            yn_u_norm_r.append(yi_u_norm/yi_c_norm)
            yn_d_norm_r.append(yi_d_norm/yi_c_norm)
        else:
            yn_u_norm_r.append(0.)
            yn_d_norm_r.append(0.)
    for i in range(0,len(yn_d_norm_r)):
        if yn_d_norm_r[i] != 0:
            yn_d_norm_r[i] = yn_d_norm_r[i] - 1.0
        else:
            yn_d_norm_r[i] = 0.
        if yn_u_norm_r[i] != 0:
            yn_u_norm_r[i] = yn_u_norm_r[i] - 1.0
        else:
            yn_u_norm_r[i] = 0.
    if lines_or_band == 'band':
       ax3.fill_between(x, yn_d_norm_r, yn_u_norm_r, facecolor=colour, interpolate=True, alpha=alphaval)
    if lines_or_band == 'lines':
        ax3.plot(x, yn_u_norm_r,linewidth=1.0, color=colour, label=lab, ls=style, marker='')
        ax3.plot(x, yn_d_norm_r,linewidth=1.0, color=colour, label='', ls=style, marker='')
        print 'x, ydown, yup', colour, ':'
        for ii in range(len(x)):
            print x[ii], yn_d_norm_r[ii], yn_u_norm_r[ii]


def hist_top_pdf_ratio_to_central_R(plot_to_make, lab, colour, style, yodafilename_zh, yodafilename_wh, xsec_zh, xsec_wh, xlabel, fig, gs, alphaval, lines_or_band, nn_min, nn_max, setlabels, ax3):
    #ax3 = fig.add_subplot(gs[3,:])
    if setlabels:
        ax3.set_xlabel(xlabel,fontsize=20)
        ax3.legend()
        ax3.legend(loc="best", numpoints=1, frameon=False, prop={'size':8})
    # get x-values from the first filename: these should all be the same
    yodafilenames_zh = []
    yodafilenames_zh.append(yodafilename_zh)
    yodafilenames_wh = []
    yodafilenames_wh.append(yodafilename_wh)
    x = []
    # pdfs
    yn_zh = {}
    yn_wh = {}
    nreps = 0
    for nn in range(nn_min,nn_max+1):
        x, xup, xdown, dx, yi_zh = hist_top_data_stack(yodafilenames_zh, plot_to_make + ' PDF ' + str(nn), lab, colour, style)
        x, xup, xdown, dx, yi_wh = hist_top_data_stack(yodafilenames_wh, plot_to_make + ' PDF ' + str(nn), lab, colour, style)

        yn_zh[nn] = yi_zh
        yn_wh[nn] = yi_wh
        nreps = nreps + 1

    ysum = []
    ysum_sq = []
    yav = []
    stddev = []
    for k in range(0,len(yn_zh[0])):
        ysum.append(0)
        ysum_sq.append(0)
        yav.append(0)
        stddev.append(0)
        for nn in range(nn_min,nn_max+1):
            if yn_wh[nn][k] != 0:
                ysum[k] = ysum[k] + yn_zh[nn][k]/yn_wh[nn][k] # the sum of values
                ysum_sq[k] = ysum_sq[k] + (yn_zh[nn][k]/yn_wh[nn][k])**2 # the sum of values
    for k in range(0,len(yn_zh[0])):
        yav[k] = ysum[k]/nreps
        stddev[k] = math.sqrt(ysum_sq[k]/nreps - yav[k]**2)
    yn_u = []
    yn_d = []
    for j in range(0,len(yn_zh[0])):
        yn_d.append(0.)
        yn_u.append(0.)
        if yav[j] != 0.:
            yn_u[j] = ((yav[j]+stddev[j])/yav[j])
            yn_d[j] = ((yav[j]-stddev[j])/yav[j])
    for i in range(0,len(yn_d)):
        if yn_d[i] != 0.:
            yn_d[i] = yn_d[i] - 1.0
        else:
            yn_d[i] = 0.
        if yn_u[i] != 0.:
            yn_u[i] = yn_u[i] - 1.0
        else:
            yn_u[i] = 0.
    if lines_or_band == 'band':
       ax3.fill_between(x, yn_d, yn_u, facecolor=colour, interpolate=True, alpha=alphaval)
    if lines_or_band == 'lines':
        print yn_u
        print yn_d
        ax3.plot(x, yn_u,linewidth=1.0, color=colour, label=lab, ls=style, marker='')
        ax3.plot(x, yn_d,linewidth=1.0, color=colour, label='', ls=style, marker='')
        print 'x, ydown, yup', colour, ':'
        for ii in range(len(x)):
            print x[ii], yn_d[ii], yn_u[ii]

def hist_top_both_ratio_to_central_R(plot_to_make, lab, colour, style, yodafilename_zh, yodafilename_wh, xsec_zh, xsec_wh, xlabel, fig, gs, alphaval, lines_or_band, nn_min, nn_max, setlabels, ax3):
    if setlabels:
        ax3.set_xlabel(xlabel,fontsize=20)
        ax3.legend()
        ax3.legend(loc="best", numpoints=1, frameon=False, prop={'size':8})
    # get x-values from the first filename: these should all be the same
    yodafilenames_zh = []
    yodafilenames_zh.append(yodafilename_zh)
    yodafilenames_wh = []
    yodafilenames_wh.append(yodafilename_wh)
    x = []
    # pdfs
    yn_zh = {}
    yn_wh = {}
    nreps = 0
    for nn in range(nn_min,nn_max+1):
        x, xup, xdown, dx, yi_zh = hist_top_data_stack(yodafilenames_zh, plot_to_make + ' PDF ' + str(nn), lab, colour, style)
        x, xup, xdown, dx, yi_wh = hist_top_data_stack(yodafilenames_wh, plot_to_make + ' PDF ' + str(nn), lab, colour, style)

        yn_zh[nn] = yi_zh
        yn_wh[nn] = yi_wh
        nreps = nreps + 1

    ysum = []
    ysum_sq = []
    yav = []
    stddev = []
    for k in range(0,len(yn_zh[0])):
        ysum.append(0)
        ysum_sq.append(0)
        yav.append(0)
        stddev.append(0)
        for nn in range(nn_min,nn_max+1):
            if yn_wh[nn][k] != 0:
                ysum[k] = ysum[k] + yn_zh[nn][k]/yn_wh[nn][k] # the sum of values
                ysum_sq[k] = ysum_sq[k] + (yn_zh[nn][k]/yn_wh[nn][k])**2 # the sum of values
    for k in range(0,len(yn_zh[0])):
        yav[k] = ysum[k]/nreps
        stddev[k] = math.sqrt(ysum_sq[k]/nreps - yav[k]**2)
  
    # scales
    x, xup, xdown, dx, yn_su_zh = hist_top_data_stack(yodafilenames_zh, plot_to_make + ' (scale, up)', lab, colour, style)
    x, xup, xdown, dx, yn_sd_zh = hist_top_data_stack(yodafilenames_zh, plot_to_make + ' (scale, down)', lab, colour, style)
    x, xup, xdown, dx, yn_sc_zh = hist_top_data_stack(yodafilenames_zh, plot_to_make + ' (scale, central)', lab, colour, style)

    x, xup, xdown, dx, yn_su_wh = hist_top_data_stack(yodafilenames_wh, plot_to_make + ' (scale, up)', lab, colour, style)
    x, xup, xdown, dx, yn_sd_wh = hist_top_data_stack(yodafilenames_wh, plot_to_make + ' (scale, down)', lab, colour, style)
    x, xup, xdown, dx, yn_sc_wh = hist_top_data_stack(yodafilenames_wh, plot_to_make + ' (scale, central)', lab, colour, style)

    yn_u = []
    yn_d = []
    yn_c = []
    yj = 0
    ysum_c = 0
    ysum_u = 0
    ysum_d = 0  
    for j in range(0,len(yn_su_zh)):
        yi_u = 0
        yi_d = 0
        yi_c = 0
        if yn_sc_wh[j] != 0.:
            yi_u = yn_sc_zh[j]/yn_sc_wh[j] + math.sqrt(stddev[j]**2 + (yn_su_zh[j]/yn_su_wh[j]-yn_sc_zh[j]/yn_sc_wh[j])**2 )
            yi_d = yn_sc_zh[j]/yn_sc_wh[j] - math.sqrt(stddev[j]**2 + (yn_sc_zh[j]/yn_sc_wh[j]-yn_sd_zh[j]/yn_sd_wh[j])**2 )
            yi_c = yn_sc_zh[j]/yn_sc_wh[j]
        ysum_u = ysum_u + yi_u
        ysum_d = ysum_d + yi_d
        ysum_c = ysum_c + yi_c
        yn_u.append(yi_u)
        yn_d.append(yi_d)
        yn_c.append(yi_c)
    yn_u_norm = []
    yn_d_norm = []
    yn_c_norm = []
    for l in range(0,len(yn_su_zh)):
        yi_u_norm = 1.
        yi_d_norm = 1.
        if yn_c[l] != 0.:
            yi_u_norm = yn_u[l] / yn_c[l]
            yi_d_norm = yn_d[l] / yn_c[l]
        yn_u_norm.append(yi_u_norm)
        yn_d_norm.append(yi_d_norm)
    #print yn_d_norm
    for i in range(0,len(yn_d_norm)):
        if yn_d_norm[i] != 0.:
            yn_d_norm[i] = yn_d_norm[i] - 1.0
        else:
            yn_d_norm[i] = 0.
        if yn_u_norm[i] != 0.:
            yn_u_norm[i] = yn_u_norm[i] - 1.0
        else:
            yn_u_norm[i] = 0.
    if lines_or_band == 'band':
       ax3.fill_between(x, yn_d_norm, yn_u_norm, facecolor=colour, interpolate=True, alpha=alphaval)
    if lines_or_band == 'lines':
        ax3.plot(x, yn_u_norm,linewidth=1.0, color=colour, label=lab, ls=style, marker='')
        ax3.plot(x, yn_d_norm,linewidth=1.0, color=colour, label='', ls=style, marker='')
    if lines_or_band == 'donotplot':
        print 'not plotting'
    return yn_u_norm, yn_d_norm


def hist_top_both_doubleratio(plot_to_make, lab, lab2, lab3, colour, style, colour2, style2, colour3, style3, yodafilename_zh, yodafilename_wh, yodafilename_ggzh, xsec_zh, xsec_wh, xsec_ggzh, xlabel, fig, gs, alphaval, lines_or_band, setlabels, ax3):
    if setlabels:
        ax3.set_xlabel(xlabel,fontsize=20)
        ax3.legend()
        ax3.legend(loc="best", numpoints=1, frameon=False, prop={'size':8})
    # get x-values from the first filename: these should all be the same
    yodafilenames_ggzh = []
    yodafilenames_ggzh.append(yodafilename_ggzh)
    yodafilenames_zh = []
    yodafilenames_zh.append(yodafilename_zh)
    yodafilenames_wh = []
    yodafilenames_wh.append(yodafilename_wh)
    x = []
 
    x, xup, xdown, dx, yn_sc_zh = hist_top_data_stack(yodafilenames_zh, plot_to_make, lab, colour, style)

    x, xup, xdown, dx, yn_sc_wh = hist_top_data_stack(yodafilenames_wh, plot_to_make, lab, colour, style)

    x, xup, xdown, dx, yn_sc_ggzh = hist_top_data_stack(yodafilenames_ggzh, plot_to_make, lab, colour, style)

    ysum_zh = 0.
    ysum_wh = 0.
    ysum_ggzh = 0.
    for j in range(0,len(yn_sc_zh)):
        ysum_zh = ysum_zh + yn_sc_zh[j]
        ysum_wh = ysum_wh + yn_sc_wh[j]
        ysum_ggzh = ysum_ggzh + yn_sc_ggzh[j]

    yn_zh_xsnorm = []
    yn_wh_xsnorm = []
    yn_ggzh_xsnorm = []

    for j in range(0,len(yn_sc_zh)):
        yn_zh_xsnorm.append(xsec_zh*yn_sc_zh[j]/ysum_zh)
        yn_wh_xsnorm.append(xsec_wh*yn_sc_wh[j]/ysum_wh)
        yn_ggzh_xsnorm.append(xsec_ggzh*yn_sc_ggzh[j]/ysum_ggzh)

    yn_RDY_c = []
    yn_RFULL_c = []
    yn_RR_c = []

    for j in range(0,len(yn_ggzh_xsnorm)):
        yi_RDY_c = 0
        yi_RR_c = 0
        yi_RFULL_c = 0
        if yn_wh_xsnorm[j] != 0.:
            yi_RDY_c = yn_zh_xsnorm[j]/yn_wh_xsnorm[j]
            yi_RFULL_c = (yn_zh_xsnorm[j]+yn_ggzh_xsnorm[j])/yn_wh_xsnorm[j]
            if yi_RFULL_c != 0.:
                yi_RR_c = yi_RDY_c/yi_RFULL_c
        yn_RDY_c.append(yi_RDY_c)
        yn_RFULL_c.append(yi_RFULL_c)
        yn_RR_c.append(yi_RR_c)
    ax3.plot(x, yn_RDY_c,linewidth=2.0, color=colour2, label=lab2, ls=style2, marker='')
    ax3.plot(x, yn_RFULL_c,linewidth=2.0, color=colour3, label=lab3, ls=style3, marker='')
    ax3.plot(x, yn_RR_c,linewidth=2.0, color=colour, label=lab, ls=style, marker='')


def hist_top_both_doubleratio_inverse(plot_to_make, lab, lab2, lab3, colour, style, colour2, style2, colour3, style3, yodafilename_zh, yodafilename_wh, yodafilename_ggzh, xsec_zh, xsec_wh, xsec_ggzh, xlabel, fig, gs, alphaval, lines_or_band, setlabels, ax4):
    if setlabels:
        ax4.set_xlabel(xlabel,fontsize=20)
        ax4.legend()
        ax4.legend(loc="best", numpoints=1, frameon=False, prop={'size':8})
    # get x-values from the first filename: these should all be the same
    yodafilenames_ggzh = []
    yodafilenames_ggzh.append(yodafilename_ggzh)
    yodafilenames_zh = []
    yodafilenames_zh.append(yodafilename_zh)
    yodafilenames_wh = []
    yodafilenames_wh.append(yodafilename_wh)
    x = []
 
    x, xup, xdown, dx, yn_sc_zh = hist_top_data_stack(yodafilenames_zh, plot_to_make, lab, colour, style)

    x, xup, xdown, dx, yn_sc_wh = hist_top_data_stack(yodafilenames_wh, plot_to_make, lab, colour, style)

    x, xup, xdown, dx, yn_sc_ggzh = hist_top_data_stack(yodafilenames_ggzh, plot_to_make, lab, colour, style)

    ysum_zh = 0.
    ysum_wh = 0.
    ysum_ggzh = 0.
    for j in range(0,len(yn_sc_zh)):
        ysum_zh = ysum_zh + yn_sc_zh[j]
        ysum_wh = ysum_wh + yn_sc_wh[j]
        ysum_ggzh = ysum_ggzh + yn_sc_ggzh[j]

    yn_zh_xsnorm = []
    yn_wh_xsnorm = []
    yn_ggzh_xsnorm = []

    for j in range(0,len(yn_sc_zh)):
        yn_zh_xsnorm.append(xsec_zh*yn_sc_zh[j]/ysum_zh)
        yn_wh_xsnorm.append(xsec_wh*yn_sc_wh[j]/ysum_wh)
        yn_ggzh_xsnorm.append(xsec_ggzh*yn_sc_ggzh[j]/ysum_ggzh)

    yn_RDY_c = []
    yn_RFULL_c = []
    yn_RR_c = []
    yn_RR_c_inverse = []
    for j in range(0,len(yn_ggzh_xsnorm)):
        yi_RDY_c = 0
        yi_RR_c = 0
        yi_RFULL_c = 0
        if yn_wh_xsnorm[j] != 0.:
            yi_RDY_c = yn_zh_xsnorm[j]/yn_wh_xsnorm[j]
            yi_RFULL_c = (yn_zh_xsnorm[j]+yn_ggzh_xsnorm[j])/yn_wh_xsnorm[j]
            if yi_RFULL_c != 0.:
                yi_RR_c = yi_RDY_c/yi_RFULL_c
        yn_RDY_c.append(yi_RDY_c)
        yn_RFULL_c.append(yi_RFULL_c)
        yn_RR_c.append(yi_RR_c)
        if yi_RR_c != 0.:
            yn_RR_c_inverse.append(1/yi_RR_c)
        else:
            yn_RR_c_inverse.append(0.)
#    ax4.plot(x, yn_RDY_c,linewidth=2.0, color=colour2, label=lab2, ls=style2, marker='')
#    ax4.plot(x, yn_RFULL_c,linewidth=2.0, color=colour3, label=lab3, ls=style3, marker='')
    print len(yn_RR_c_inverse),yn_RR_c_inverse
    ax4.plot(x, yn_RR_c_inverse,linewidth=2.0, color=colour, label=lab, ls=style, marker='')

def hist_top_both_doubleratio_inverse_nowh(plot_to_make, lab, lab2, lab3, colour, style, colour2, style2, colour3, style3, yodafilename_zh, yodafilename_ggzh, xsec_zh, xsec_ggzh, xlabel, fig, gs, alphaval, lines_or_band, setlabels, ax4):
    if setlabels:
        ax4.set_xlabel(xlabel,fontsize=20)
        ax4.legend()
        ax4.legend(loc="best", numpoints=1, frameon=False, prop={'size':8})
    # get x-values from the first filename: these should all be the same
    yodafilenames_ggzh = []
    yodafilenames_ggzh.append(yodafilename_ggzh)
    yodafilenames_zh = []
    yodafilenames_zh.append(yodafilename_zh)
    x = []
 
    x, xup, xdown, dx, yn_sc_zh = hist_top_data_stack(yodafilenames_zh, plot_to_make, lab, colour, style)

    x, xup, xdown, dx, yn_sc_ggzh = hist_top_data_stack(yodafilenames_ggzh, plot_to_make, lab, colour, style)
    #print yn_sc_zh

    ysum_zh = 0.
    ysum_ggzh = 0.
    for j in range(0,len(yn_sc_zh)):
        ysum_zh = ysum_zh + yn_sc_zh[j]
        ysum_ggzh = ysum_ggzh + yn_sc_ggzh[j]

    yn_zh_xsnorm = []
    yn_ggzh_xsnorm = []

    for j in range(0,len(yn_sc_zh)):
        yn_zh_xsnorm.append(xsec_zh*yn_sc_zh[j]/ysum_zh)
        yn_ggzh_xsnorm.append(xsec_ggzh*yn_sc_ggzh[j]/ysum_ggzh)

    yn_RDY_c = []
    yn_RFULL_c = []
    yn_RR_c = []
    yn_RR_c_inverse = []
    for j in range(0,len(yn_ggzh_xsnorm)):
        yi_RDY_c = 0
        yi_RR_c = 0
        yi_RFULL_c = 0
        yi_RDY_c = yn_zh_xsnorm[j]/1.
        yi_RFULL_c = (yn_zh_xsnorm[j]+yn_ggzh_xsnorm[j])/1.
        if yi_RFULL_c != 0.:
            yi_RR_c = yi_RDY_c/yi_RFULL_c
        yn_RDY_c.append(yi_RDY_c)
        yn_RFULL_c.append(yi_RFULL_c)
        yn_RR_c.append(yi_RR_c)
        if yi_RR_c != 0.:
            yn_RR_c_inverse.append(1/yi_RR_c)
        else:
            yn_RR_c_inverse.append(0.)
#    ax4.plot(x, yn_RDY_c,linewidth=2.0, color=colour2, label=lab2, ls=style2, marker='')
#    ax4.plot(x, yn_RFULL_c,linewidth=2.0, color=colour3, label=lab3, ls=style3, marker='')
    print len(yn_RR_c_inverse),yn_RR_c_inverse
    ax4.plot(x, yn_RR_c_inverse,linewidth=2.0, color=colour, label=lab, ls=style, marker='')

def hist_top_both_doubleratio_oneplus(plot_to_make, lab, lab2, lab3, colour, style, colour2, style2, colour3, style3, yodafilename_zh, yodafilename_wh, yodafilename_ggzh, xsec_zh, xsec_wh, xsec_ggzh, xlabel, fig, gs, alphaval, lines_or_band, setlabels, ax4):
    if setlabels:
        ax4.set_xlabel(xlabel,fontsize=20)
        ax4.legend()
        ax4.legend(loc="best", numpoints=1, frameon=False, prop={'size':8})
    # get x-values from the first filename: these should all be the same
    yodafilenames_ggzh = []
    yodafilenames_ggzh.append(yodafilename_ggzh)
    yodafilenames_zh = []
    yodafilenames_zh.append(yodafilename_zh)
    yodafilenames_wh = []
    yodafilenames_wh.append(yodafilename_wh)
    x = []
 
    x, xup, xdown, dx, yn_sc_zh = hist_top_data_stack(yodafilenames_zh, plot_to_make, lab, colour, style)

    x, xup, xdown, dx, yn_sc_wh = hist_top_data_stack(yodafilenames_wh, plot_to_make, lab, colour, style)

    x, xup, xdown, dx, yn_sc_ggzh = hist_top_data_stack(yodafilenames_ggzh, plot_to_make, lab, colour, style)

    
    yn_RR_c = np.tile(1., len(x)) + divide_arrays(mult_array(yn_sc_ggzh,xsec_ggzh), mult_array(yn_sc_zh,xsec_zh))
    
    
    ax4.plot(x, yn_RR_c,linewidth=2.0, color=colour, label=lab, ls=style, marker='')
    print 'x, yn_RR_c', colour
    for ii in range(len(x)):
        print x[ii], yn_RR_c[ii]

def hist_top_both_rzw(plot_to_make, lab, lab2, lab3, colour, style, colour2, style2, colour3, style3, yodafilename_zh, yodafilename_wh, yodafilename_ggzh, xsec_zh, xsec_wh, xsec_ggzh, xlabel, fig, gs, alphaval, lines_or_band, setlabels, ax4):
    if setlabels:
        ax4.set_xlabel(xlabel,fontsize=20)
        ax4.legend()
        ax4.legend(loc="best", numpoints=1, frameon=False, prop={'size':8})
    # get x-values from the first filename: these should all be the same
    yodafilenames_ggzh = []
    yodafilenames_ggzh.append(yodafilename_ggzh)
    yodafilenames_zh = []
    yodafilenames_zh.append(yodafilename_zh)
    yodafilenames_wh = []
    yodafilenames_wh.append(yodafilename_wh)
    x = []
 
    x, xup, xdown, dx, yn_sc_zh = hist_top_data_stack(yodafilenames_zh, plot_to_make, lab, colour, style)

    x, xup, xdown, dx, yn_sc_wh = hist_top_data_stack(yodafilenames_wh, plot_to_make, lab, colour, style)

    x, xup, xdown, dx, yn_sc_ggzh = hist_top_data_stack(yodafilenames_ggzh, plot_to_make, lab, colour, style)

    
    yn_RZW_c = divide_arrays(mult_array(yn_sc_zh,xsec_zh), mult_array(yn_sc_wh,xsec_wh))
    print yn_RZW_c
    
    ax4.plot(x, yn_RZW_c,linewidth=2.0, color=colour, label=lab, ls=style, marker='')
    print 'x, yn_RZW_c:', colour
    for ii in range(len(x)):
        print x[ii], yn_RZW_c[ii]


def hist_top_lumi_data_stack(yodafilenames, plot_to_make, lab, colour, style, xsecs, luminosity):
    yn_all = {}
    yn_stacked = []
    plot_to_make = '"' + plot_to_make + '"'
    for yodafilename in yodafilenames:
        xsec = xsecs[yodafilename]
        infile = open(yodafilename,"r")
        reading = False
        plot_found = False
        inputfile = yodafilename
        #sum = {}
        sum = defaultdict(lambda: 0)
        xlo_d = defaultdict(lambda: 0)
        xhi_d = defaultdict(lambda: 0)
        deltax_d = defaultdict(lambda: 0)
        errsq = defaultdict(lambda: 0)
        print plot_to_make, yodafilename
        xav = -9999
        dxi = -9999
        for line in infile:
            #print line
            if 'HIST' in line and reading is True:
                reading = False
                break
            if reading:
                #print line
                val = float(line.split()[1])
                xav_old = xav
                xav = float(line.split()[0])
                if xav_old is not -9999:
                    dxi = 0.5*(xav-xav_old)
               # print dxi
                sum[xav] = val
                #print xav, yav
                #print xav, yav
            if 'SET LIMITS X' in line and plot_found is True:
                reading = True
            if 'TITLE TOP' in line and plot_to_make in line:
                plot_found = True
                print 'plot found:', plot_to_make
        yn = []
        x = []
        y = []
        xup = []
        xdown = []
        dx = []
        ysum = 0
        sumord = OrderedDict(sorted(sum.items(), key=lambda t: t[0]))
        for s in sumord:
            #print s, sumord[s]
            x.append(s)
            xup.append(s+dxi)
            xdown.append(s-dxi)
            dx.append(dxi)
            y.append(sum[s])
            ysum = ysum + sum[s]
        # normalize:
        errn = []
        ii = 0
        if ysum == 0: print 'WARNING, plot', plot_to_make, 'in', yodafilename, 'yields zero ysum!'
        for yi in y:
            yi = luminosity*xsec*yi/ysum
            yn.append(yi)
            ii = ii + 1
        if plot_found is False:
            print 'WARNING, plot', plot_to_make, 'not found in', yodafilename, '!'
        yn_all[yodafilename] = yn
        not_resized = True
    for key in yn_all:
        jj = 0
        for yjj in yn_all[key]:
            if not_resized:
                for ll in range(0,len(yn_all[key])):
                    yn_stacked.append(0.)
                not_resized = False
            yn_stacked[jj] = yn_stacked[jj] + yn_all[key][jj]
            jj = jj + 1
    return x, xup, xdown, dx, yn_stacked


def square(list):
    return [i ** 2 for i in list]

def square_root(list):
    newlist = []
    for i in list:
        if i < 0:
            i = 0.
        newlist.append(math.sqrt(i))
    return newlist

def sum_array(list1, list2):
    newlist = []
    if len(list1) != len(list2):
        print 'error, list1 does not have the same length as list 2' 
    for i in range(0,len(list1)):
        newlist.append(list1[i]+list2[i])
    return newlist

def sum_arrays(list1, list2):
    newlist = []
    if len(list1) != len(list2):
        print 'error, list1 does not have the same length as list 2' 
    for i in range(0,len(list1)):
        newlist.append(list1[i]+list2[i])
    return newlist

def average_arrays(list1, list2):
    newlist = []
    if len(list1) != len(list2):
        print 'error, list1 does not have the same length as list 2' 
    for i in range(0,len(list1)):
        newlist.append(abs(list1[i]-list2[i])/2.)
    return newlist

def mult_array(list, fac):
    newlist = []
    for i in range(0,len(list)):
        newlist.append(list[i]*fac)
    return newlist

def mult_arrays(list1, list2):
    newlist = []
    if len(list1) != len(list2):
        print 'error, list1 does not have the same length as list 2' 
    for i in range(0,len(list1)):
        newlist.append(list1[i]*list2[i])
    return newlist

def multiply_arrays(list1, list2):
    newlist = []
    if len(list1) != len(list2):
        print 'error, list1 does not have the same length as list 2' 
    for i in range(0,len(list1)):
        newlist.append(list1[i]*list2[i])
    return newlist

def bin_sum(list1):
    binsum = 0.
    for i in range(0,len(list1)):
        binsum = binsum + list1[i]
    return binsum

def oneover_array(list):
    newlist = []
    for i in range(0,len(list)):
        if list[i] != 0:
            newlist.append(1./list[i])
        else:
            newlist.append(0.)
    return newlist

def sum_array_to_number(list,num):
    newlist = []
    for i in range(0,len(list)):
        newlist.append(list[i]+num)
    return newlist


def divide_arrays(list1, list2):
    newlist = []
    if len(list1) != len(list2):
        print 'error, list1 does not have the same length as list 2' 
    for i in range(0,len(list1)):
        if list2[i] != 0.:
            newlist.append(list1[i]/list2[i])
        else:
            newlist.append(0.)
    return newlist
