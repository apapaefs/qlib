#! /usr/bin/env python

import sys, os, copy
from math import sqrt
from subprocess import Popen
from optparse import OptionParser
import lighthisto

def getTemplate(basename):
    with open('%s.template' % basename, 'r') as f:
        templateText = f.read()
    return string.Template( templateText )

parser = OptionParser(usage="%prog [dat file used as template] [dat file ...]")

parser.add_option("-o", "--outfile", dest="OUTFILE",
                  default="bands.dat", help="file .dat output.")

parser.add_option("-e", "--exclude", dest="EXCLUDE",
                  default="-1", help="which histogram to exclude from band generation")

parser.add_option("-b", "--base", dest="BASE",
                  default="-1", help="which histogram to use as a base")

parser.add_option("-m", "--mcerror", dest="MCERROR",
                  default=0, help="include Monte Carlo error or not")


opts, args = parser.parse_args()

exc = int(opts.EXCLUDE)
base = int(opts.BASE)
mcerror=int(opts.MCERROR)

templ_created = False
num_histo = 0
headerdata = []
current_hdata = []
current_hist = []
current_name = ''
hist = {}
num_histo = 0
nlines = 0
ndata = 0

for datfile in args:
    print 'Creating template from', datfile
    infile = open(datfile,"r")
    for line in infile:
        if '# END HISTOGRAM' in line:
            num_histo += 1
        if templ_created is False and 'DrawOnly' not in line and 'RatioPlotReference' not in line and 'RatioPlot' not in line and '# END PLOT' not in line:
            headerdata.append(line)             
        if '# END PLOT' in line:
                templ_created = True
    
    
    nh = 0
    infile = open(datfile,"r")
    for line in infile:
        #if line.split():
        #    print line.split()[0]
        if '# END HISTOGRAM' in line:
            current_name = datfile.replace('dat','') + str(nh);
            hist[current_name] = current_hist
            nh += 1 
            current_hist = []
            nlines = ndata
            ndata = 0
            #if line.split():
            #print line[0], len(line.split()), ((line[0])).isalpha()
        if (line[0]).isalpha() is False and len(line.split()) is 5:
            current_hist.append(' '.join((line.split()[0],line.split()[1],line.split()[2], line.split()[3], line.split()[4])))
            ndata += 1
    
            #for i in headerdata:
    #    print i
    #print hist

print 'Number of Histograms:', num_histo
if exc is not -1:
    print 'Excluding histogram number', exc, 'and creating one histogram out of the rest'
if exc is -1:
    print 'Creating one histogram'

mc = 0
if mcerror is 1:
    print 'Including Monte Carlo error'
    mc = 1

# first find the maximum and minimum <value> 
minvalue = []
maxvalue = []
value = []
xmin = []
xmax = []
for x in range(nlines):
    minvalue.append(10000000000000.)
    maxvalue.append(-10000000000000.)
    value.append(0.)
    xmin.append(0)
    xmax.append(1)
xx = 0
for j in range(num_histo):
    if j is not exc:
        xx = 0
        hist_name = datfile.replace('dat','') + str(j);
        hist_search = hist[hist_name]
        for p in hist_search:
            if float(p.split()[2])+mc*float(p.split()[4]) > maxvalue[xx]:
                maxvalue[xx] = float(p.split()[2])+mc*float(p.split()[4])
            if float(p.split()[2])-mc*float(p.split()[3]) < minvalue[xx]:
                minvalue[xx] = float(p.split()[2])-mc*float(p.split()[3]) 
            xmin[xx] = float(p.split()[0])
            xmax[xx] = float(p.split()[1])
            xx += 1
            #print float(p.split()[2])

ll=0
if base is -1:
    for xxx in range(nlines):
        value[xxx] = 0.5* (minvalue[xxx]+maxvalue[xxx])
if base is not -1:
    hist_name = datfile.replace('dat','') + str(base);
    hist_search = hist[hist_name]
    for p in hist_search:
        value[ll] = float(p.split()[2])
        ll += 1

        #for ll in range(nlines):
        # print value[ll], abs(minvalue[ll]-value[ll]), abs(maxvalue[ll]-value[ll])

try:
    outdat = open(opts.OUTFILE, "w")
except:
    sys.stderr.write("Couldn't open outfile %s for writing." % opts.OUTFILE)

for i in headerdata:
    outdat.write(i)
    
if exc is -1:
    outdat.write('DrawOnly=' +  datfile.replace('dat','') + 'all\n')
if exc is not -1 and base is -1:
    outdat.write('DrawOnly= ' +  datfile.replace('dat','') + 'all ' + datfile.replace('dat','') + str(exc) + '\n')
    outdat.write('RatioPlot=1\n')
    outdat.write('RatioPlotReference=' + datfile.replace('dat','') + str(exc) + '\n')
    outdat.write('RatioPlotYLabel=Ratio\n')
    outdat.write('RatioPlotYMin=0.1\n')
    outdat.write('RatioPlotYMax=1.9\n')
    outdat.write('RatioPlotErrorBandColor=red\n')
    outdat.write('RatioPlotSameStyle=1\n')
    outdat.write('RatioPlotYLabel=Ratio\n')
if base is not -1 and exc is -1:
    outdat.write('DrawOnly= ' +  datfile.replace('dat','') + 'all ' + datfile.replace('dat','') + str(base) + '\n')
    outdat.write('RatioPlot=1\n')
    outdat.write('RatioPlotReference=' + datfile.replace('dat','') + str(base) + '\n')
    outdat.write('RatioPlotYLabel=Ratio\n')
    outdat.write('RatioPlotYMin=0.1\n')
    outdat.write('RatioPlotYMax=1.9\n')
    outdat.write('RatioPlotSameStyle=1\n')
    outdat.write('RatioPlotYLabel=Ratio\n')
    outdat.write('Legend=0\n')
outdat.write('# END PLOT\n')
outdat.write('\n')

for k in range(num_histo):
    if k is exc:
        hist_name = datfile.replace('dat','') + str(k);
        hist_search = hist[hist_name]
        outdat.write('# BEGIN HISTOGRAM ' + hist_name + '\n')
        outdat.write('ErrorBands=1\n')
        outdat.write('LineColor=red\n')
        outdat.write('ErrorBandOpacity=0.5\n')
        outdat.write('ErrorBandColor=red\n')
        outdat.write('LineStyle=solid\n')
        title = 'Title=' + hist_name + '\n'
        outdat.write(title)
        for p in hist_search:
            outdat.write(p + '\n')
        outdat.write('# END HISTOGRAM\n\n')
    if k is base:
        hist_name = datfile.replace('dat','') + str(k);
        hist_search = hist[hist_name]
        outdat.write('# BEGIN HISTOGRAM ' + hist_name + '\n')
        outdat.write('ErrorBands=0\n')
        outdat.write('ErrorBars=1\n')
        outdat.write('LineColor=blue\n')
        outdat.write('ErrorBandOpacity=0.5\n')
        outdat.write('ErrorBandColor=blue\n')
        outdat.write('LineStyle=solid\n')
        title = 'Title=' + hist_name + '\n'
        outdat.write(title)
        for p in hist_search:
            outdat.write(p + '\n')
        outdat.write('# END HISTOGRAM\n\n')

        
title =  datfile.replace('dat','') +'all'
outdat.write('# BEGIN HISTOGRAM ' + title + '\n') 
title = 'Title='+ datfile.replace('dat','') +'all\n'
outdat.write('ErrorBands=1\n')
outdat.write('ErrorBandOpacity=0.5\n')
outdat.write('ErrorBandColor=blue\n')
outdat.write('LineColor=blue\n')
outdat.write('LineStyle=solid\n')
outdat.write(title)
for yy in range(nlines):
    lineyy = str(xmin[yy]) + ' ' + str(xmax[yy]) + ' ' + str(value[yy]) + ' ' + str(abs(minvalue[yy]-value[yy])) + ' ' + str(abs(maxvalue[yy]-value[yy])) + '\n'
    outdat.write(lineyy)
outdat.write('# END HISTOGRAM\n')
        
