#! /usr/bin/env python
from __future__ import with_statement
import cmath, string, os, sys, fileinput, pprint, math
from optparse import OptionParser
import subprocess
from scipy import special
from scipy import stats
import scipy.stats
import numpy as np
from scipy.stats import norm
from scipy.stats import invgamma
import random
import sys
import time
#import datetime2
import os.path

parser = OptionParser(usage="%prog gen or run")

opts, args = parser.parse_args()

print('calculate the expected exclusion in p-value and sigmas\n')

if len(args) < 2:
    print('usage: excpoisson [signal] [bacgkround]')
    exit()

#print args
    
signal = float(args[0])
background = float(args[1])

p = 1-special.gammaincc(background, signal+background)
z = norm.ppf(p)
print('p-value = ', p)
print('possible to exclude at', z, 'sigmas')
