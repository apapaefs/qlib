#! /usr/bin/env python
from __future__ import with_statement
import cmath, string, os, sys, fileinput, pprint, math
from optparse import OptionParser
import subprocess
from scipy import special
from scipy import stats
import scipy.stats
import numpy as np
from scipy.stats import invgamma
import random
import sys
import time
import datetime
import os.path

parser = OptionParser(usage="%prog gen or run")

opts, args = parser.parse_args()

print('calculate N-sigma interval for given number number of events\n')

if len(args) < 2:
    print('usage: sigpoisson [Number of events] [Sigmas]')
    exit()

#print args
    
N = float(args[0])
S = float(args[1])

y = (1 - scipy.special.ndtr(S))
#print y
a =N+1

print('the', S, '-sigma interval for', N, 'events is given by:')

#print 'p = ',y, 'N(p) = ',scipy.special.gammaincinv(a,y) 
print('N =',N, '+', (N-scipy.special.gammaincinv(a,y)), '-', (scipy.special.gammaincinv(a,1-y)-N),)

print('\n[compare to Gaussian approx:')
print('N =',N, '+', math.sqrt(N), '-', math.sqrt(N))

