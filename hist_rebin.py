#! /usr/bin/env python

import sys, os, copy
from math import sqrt
from subprocess import Popen
from optparse import OptionParser


parser = OptionParser(usage="%prog [file to rebin] [file output]")

parser.add_option("-o", "--outfile", dest="OUTFILE",
                  default="rebinned.dat", help="file for output.")

parser.add_option("-f", "--factor", dest="FACTOR",
                  default="2", help="rebin factor.")

parser.add_option("-n", "--unnorm", dest="NORM",
                  default=0, help="whether to maintain norm.")




opts, args = parser.parse_args()


if len(args) < 1:
    sys.stderr.write("An input file (2 or 3 columns), must be specified\n")
    sys.exit(1)



line1 = []
line2 = []
line3 = []

line1_rb = []
line2_rb = []
line3_rb  = []

# read file
ii = 0
for topfile in args:
    print 'rebinning', topfile
    infile = open(topfile,"r")
    for line in infile:
        #print line
        ii += 1
        if(len(line.split()) > 0):
            line1.append(line.split()[0])
        if(len(line.split()) > 1):
            line2.append(line.split()[1])
        if(len(line.split()) > 2):
            line3.append(line.split()[2])
        else:
            line3.append('0')

print 'Rebinning by', opts.FACTOR
n = int(opts.FACTOR)
norm = opts.NORM


#print len(line1), n, float(len(line1))/float(n)
if len(line1)%n != 0:
    print 'Error: number of bins over rebinning factor is not an integer'
    exit()
    #   print float(len(line1))/float(n), 'is an integer'

# rebin by factor of n
for kk in range(0,len(line1)/n):
    dline1 = 0.
    dline2 = 0.
    dline3 = 0.
    for xx in range(n * kk, n * kk + n):
        dline1 += float(line1[xx])
        dline2 += float(line2[xx])
        dline3 += float(line3[xx])**2
    dline1 /= float(n)
    dline3 = sqrt(dline3)
    if(norm):
        dline2 /= float(n)
        dline3 /= sqrt(float(n))
    line1_rb.append(str(dline1))
    line2_rb.append(str(dline2))
    line3_rb.append(str(dline3))
    
# rebin by factor of 2
#for kk in range(0,len(line1)/2):
#    line1_rb.append(str(0.5 * (float(line1[2 * kk]) + float(line1[2 * kk+1]))))
#    line2_rb.append(str(0.5 * (float(line2[2 * kk]) + float(line2[2 * kk+1]))))
    # if(len(line3) > 0):
    #      line3_rb.append(str(0.5 * sqrt((float(line3[2 * kk]))**2 + (float(line3[2 * kk+1]))**2)))
            
print 'Writing to', opts.OUTFILE

try:
    output = open(opts.OUTFILE, "w")
except:
    sys.stderr.write("Couldn't open outfile %s for writing." % opts.OUTFILE)

for jj in range(0,len(line1_rb)):
    output.write(line1_rb[jj] + '\t' + line2_rb[jj] + '\t' + line3_rb[jj] + '\n')

print 'done'
