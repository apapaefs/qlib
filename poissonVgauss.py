import scipy
import numpy as np

# Fix signal and background:
S = 5
B = 25

# print them
print('S, B=',S,B)

# the gaussian probability to obtain S+B events given that we expect B events:
print('Gaussian probability to obtain S+B events given that we expect B events=',scipy.stats.norm.pdf(S,scale=np.sqrt(B)))
print('This corresponds to S/sqrt(B)=', S/np.sqrt(B))
print('in terms of C.L. interval p-value: 1 - 2 * scipy.special.ndtr(-S/np.sqrt(B))=',1 - 2 * scipy.special.ndtr(-S/np.sqrt(B)))
print('\n')
# Poisson:
print('in terms of C.L. interval p-value: (Poisson, upper incomplete gamma function)', 1-2*scipy.special.gammaincc(B, S+B))
print('in terms of C.L. interval p-value: (Poisson, cumulative distribution function)', 2*scipy.special.pdtr(S+B,B)-1)
