import sys
import pylab as pl
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colors

# check if string is a number
def is_number(string):
    try:
        float(string)
        return True
    except ValueError:
        return False

# choose the next colour -- for plotting
ccount = 0
def next_color():
    global ccount
    colors = ['green', 'orange', 'red', 'blue', 'black', 'cyan', 'magenta', 'brown', 'violet'] # 9 colours
    color_chosen = colors[ccount]
    if ccount < 8:
        ccount = ccount + 1
    else:
        ccount = 0    
    return color_chosen

# do not increment colour in this case:
def same_color():
    global ccount
    colors = ['green', 'orange', 'red', 'blue', 'black', 'cyan', 'magenta', 'brown', 'violet'] # 9 colours
    color_chosen = colors[ccount-1]
    return color_chosen

if len(sys.argv) < 3:
    print('histogram_values.py [output filename] [input files in single column format separated by spaces] [number of bins]')
set_n_bins = False

filename = str(sys.argv[1])

argvs = sys.argv[2:]
# remove the last one if an integer:
if is_number(sys.argv[-1]):
    n_bins_toset = int(sys.argv[-1])
    print("setting number of bins:", n_bins_toset)
    set_n_bins = True
    # remove the last entry from argvs:
    argvs = sys.argv[2:-1]
    print(argvs)

inputfiles = []
for ifile in range(len(argvs)):
    inputfiles.append(argvs[ifile])
    
print('Reading', inputfiles)

def hist_file(inputname):
    # Histogram values from a column file
    datalist = [ ( pl.loadtxt(inputname), 'input' ) ]
    for data, label in datalist:
        x = data
    axs.hist(x, bins=n_bins, histtype='step', label=inputname, color=next_color(), density=True)


# set the number of bins
n_bins=20
if set_n_bins is True:
    n_bins = n_bins_toset

fig, axs = plt.subplots(1, 1, sharey=True, tight_layout=True)
for inputfile in inputfiles:
    hist_file(inputfile)
plt.legend(loc="upper left")

#plt.show()

# save the figure in PDF format
plt.savefig(filename + '.pdf', bbox_inches='tight')

    
