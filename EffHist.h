#include <fstream>
#include <cmath>
#include <iomanip>


class EffHist {
 public:
  EffHist(); //constructor
  EffHist(int bins, string output, string title, double xmin, double xmax);
  void plot(bool log);
  void plot(bool norm, bool log);

  void add(string output, bool log);
  void add(string output, bool norm, bool log);
  
  void thfill(double value);
  void thnote(double value);

  double entries[10000];
  double tentries[10000];
  double errors[10000];

  double totalentries;
  int size;
  string output_t;
  string title_t;
  double xmin_t;
  double xmax_t;
  int bins_t;
  double interval_t; 
};




EffHist::EffHist(int bins, string output, string title, double xmin, double xmax): output_t(output), title_t(title), xmin_t(xmin), xmax_t(xmax), bins_t(bins) {
  interval_t = (xmax_t - xmin_t) / bins_t;
  for(int ddd = 0; ddd < bins_t; ddd++) { entries[ddd] = 0; tentries[ddd] = 0; errors[ddd] = 0; }
  totalentries = 0;
  interval_t = (xmax - xmin) / bins_t;
}

void EffHist::plot(bool norm, bool log) {
  double totnum = 0;
  string titlecase = "";
  string left = "";
  string leftcase = "";
  double *plotarray = new double[bins_t];
  for(int jj = 0; jj < bins_t; jj++) { 
    totnum += entries[jj];
  }  
  cout << title_t << ", N(attempted) = " << totalentries << ", N(in histo) = " << totnum << endl;
  for(int kk = 0; kk < bins_t; kk++) { 
    if(!norm) { tentries[kk] = 1; }
    plotarray[kk] = entries[kk]/tentries[kk];
    errors[kk] = plotarray[kk] * sqrt( (1/entries[kk]) + (1/tentries[kk])); 
  }
  ofstream outf;
  outf.open(output_t.c_str());
  outf << "NEW FRAME" << endl;
  outf << "SET WINDOW X 1.6 8 Y 1.6 9" << endl;
  outf << "SET FONT DUPLEX" << endl;
  outf << "TITLE TOP \""    << title_t     << "\"\n";
  outf << "CASE      \""    << titlecase << "\"\n";
  outf << "TITLE LEFT \""   << left      << "\"\n";
  outf << "CASE       \""   << leftcase  << "\"\n";
  outf << "SET INTENSITY 4\n";
  outf << "SET ORDER X Y DY\n";
  if(log) { outf << "SET SCALE Y LOG\n"; }
  outf << "SET LIMITS X " << xmin_t << " " << xmax_t << endl;
  // cout << interval << endl;
  for(int ii = 0; ii < bins_t; ii++) { 
    outf << ii * interval_t + xmin_t + interval_t * 0.5 << "\t" << plotarray[ii] << "\t" << errors[ii] << endl;
  }
  outf << "plot  " << endl;
  outf << "hist  " << endl;

  outf.close();
}


void EffHist::plot(bool log) {
  double totnum = 0;
  string titlecase = "";
  string left = "";
  string leftcase = "";
  double *plotarray = new double[bins_t];
  for(int jj = 0; jj < bins_t; jj++) { 
    totnum += entries[jj];
  }  
  cout << title_t << ", N(attempted) = " << totalentries << ", N(in histo) = " << totnum << endl;
  for(int kk = 0; kk < bins_t; kk++) { 
    plotarray[kk] = entries[kk]/tentries[kk];
    errors[kk] = plotarray[kk] * sqrt( (1/entries[kk]) + (1/tentries[kk])); 
  }
  ofstream outf;
  outf.open(output_t.c_str());
  outf << "NEW FRAME" << endl;
  outf << "SET WINDOW X 1.6 8 Y 1.6 9" << endl;
  outf << "SET FONT DUPLEX" << endl;
  outf << "TITLE TOP \""    << title_t     << "\"\n";
  outf << "CASE      \""    << titlecase << "\"\n";
  outf << "TITLE LEFT \""   << left      << "\"\n";
  outf << "CASE       \""   << leftcase  << "\"\n";
  outf << "SET INTENSITY 4\n";
  outf << "SET ORDER X Y DY\n";
  if(log) { outf << "SET SCALE Y LOG\n"; }
  outf << "SET LIMITS X " << xmin_t << " " << xmax_t << endl;
  // cout << interval << endl;
  for(int ii = 0; ii < bins_t; ii++) { 
    outf << ii * interval_t + xmin_t + interval_t * 0.5 << "\t" << plotarray[ii] << "\t" << errors[ii] << endl;
  }
  outf << "plot  " << endl;
  outf << "hist  " << endl;
  outf.close();
}

void EffHist::thfill(double value) { 
  totalentries++;
  if(value < xmax_t && value > xmin_t) {       
    // cout << "value " << value << endl;
    for(int ii = 0; ii < bins_t; ii++) { 
      if(value <= ((ii+1) * interval_t + xmin_t) && value > (ii * interval_t + xmin_t) ) {
	//	cout << value << " title = " << title_t << endl;
	entries[ii]++;
	tentries[ii]++;
	//cout << "Adding to " << ii * interval_t + xmin_t << endl;
      }
    }
  }
}
void EffHist::thnote(double value) { 
  totalentries++;
  if(value < xmax_t && value > xmin_t) {       
    // cout << "value " << value << endl;
    for(int ii = 0; ii < bins_t; ii++) { 
      if(value <= ((ii+1) * interval_t + xmin_t) && value > (ii * interval_t + xmin_t) ) {
	//	cout << value << " title = " << title_t << endl;
	tentries[ii]++;
	//cout << "Adding to " << ii * interval_t + xmin_t << endl;
      }
    }
  }
}
void EffHist::add(string output, bool norm, bool log) {
  double totnum = 0;
  string titlecase = "";
  string left = "";
  string leftcase = "";
  double *plotarray = new double[bins_t];
  for(int jj = 0; jj < bins_t; jj++) { 
    totnum += entries[jj];
  }
  cout << title_t << ", N(attempted) = " << totalentries << ", N(in histo) = " << totnum << endl;
  if(!norm) { totnum = 1; }
  for(int kk = 0; kk < bins_t; kk++) { 
    if(!norm) { tentries[kk] = 1; }
    plotarray[kk] = entries[kk]/tentries[kk];
    errors[kk] = plotarray[kk] * sqrt( (1/entries[kk]) + (1/tentries[kk])); 
  }
  ofstream outf;
  outf.open(output.c_str(), fstream::app);
  outf << "NEW FRAME" << endl;
  outf << "SET WINDOW X 1.6 8 Y 1.6 9" << endl;
  outf << "SET FONT DUPLEX" << endl;
  outf << "TITLE TOP \""    << title_t     << "\"\n";
  outf << "CASE      \""    << titlecase << "\"\n";
  outf << "TITLE LEFT \""   << left      << "\"\n";
  outf << "CASE       \""   << leftcase  << "\"\n";
  outf << "SET INTENSITY 4\n";
  outf << "SET ORDER X Y DY\n";
  if(log) { outf << "SET SCALE Y LOG\n"; }
  outf << "SET LIMITS X " << xmin_t << " " << xmax_t << endl;
  double interval = (xmax_t - xmin_t) / bins_t;
  // cout << interval << endl;
  for(int ii = 0; ii < bins_t; ii++) { 
    outf << ii * interval_t + xmin_t + interval_t * 0.5 << "\t" << plotarray[ii] << "\t" << errors[ii] << endl;
  }
  outf << "HIST  " << endl;
  outf << "plot " << endl;
  outf.close();
}
void EffHist::add(string output, bool log) {
  double totnum = 0;
  string titlecase = "";
  string left = "";
  string leftcase = "";
  double *plotarray = new double[bins_t];
  for(int jj = 0; jj < bins_t; jj++) { 
    totnum += entries[jj];
  }
  cout << title_t << ", N(attempted) = " << totalentries << ", N(in histo) = " << totnum << endl;
  for(int kk = 0; kk < bins_t; kk++) { 
    plotarray[kk] = entries[kk]/tentries[kk];
    errors[kk] = plotarray[kk] * sqrt( (1/entries[kk]) + (1/tentries[kk])); 
  }
  ofstream outf;
  outf.open(output.c_str(), fstream::app);
  outf << "NEW FRAME" << endl;
  outf << "SET WINDOW X 1.6 8 Y 1.6 9" << endl;
  outf << "SET FONT DUPLEX" << endl;
  outf << "TITLE TOP \""    << title_t     << "\"\n";
  outf << "CASE      \""    << titlecase << "\"\n";
  outf << "TITLE LEFT \""   << left      << "\"\n";
  outf << "CASE       \""   << leftcase  << "\"\n";
  outf << "SET INTENSITY 4\n";
  outf << "SET ORDER X Y DY\n";
  if(log) { outf << "SET SCALE Y LOG\n"; }
  outf << "SET LIMITS X " << xmin_t << " " << xmax_t << endl;
  double interval = (xmax_t - xmin_t) / bins_t;
  // cout << interval << endl;
  for(int ii = 0; ii < bins_t; ii++) { 
    outf << ii * interval_t + xmin_t + interval_t * 0.5 << "\t" << plotarray[ii] << "\t" << errors[ii] << endl;
  }
  outf << "HIST  " << endl;
  outf << "plot " << endl;
  outf.close();
}
