#! /usr/bin/env python

import sys, os, copy
from math import sqrt
from subprocess import Popen
import lighthisto

## Try to load faster but non-standard cElementTree module
try:
    import xml.etree.cElementTree as ET
except ImportError:
    try:
        import cElementTree as ET
    except ImportError:
        try:
            import xml.etree.ElementTree as ET
        except:
            sys.stderr.write("Can't load the ElementTree XML parser: please install it!\n")
            sys.exit(1)

from optparse import OptionParser
parser = OptionParser(usage="%prog aidafile [aidafile2 ...]")
parser.add_option("-o", "--outfile", dest="OUTFILE",
                  default="merged.aida", help="file for merged aida output.")
parser.add_option("-w", "--weight", action="append", type="float",
                  help="each given file is rescaled by the associated\n"+
                       "weight factor divided by the sum of all such\n"+
                       "weights in the combination.")
parser.add_option("-s", "--scalefactor", action="append", type="float",
                  help="each given file is rescaled by the associated\n"+
                       "scale factor.")

opts, args = parser.parse_args()
headerprefix = ""

if len(args) < 1:
    sys.stderr.write("Must specify at least one AIDA histogram file\n")
    sys.exit(1)

if (opts.weight):
    if len(opts.weight)!=len(args):
        sys.stderr.write("\n")
        sys.stderr.write("The number of aida files given is not the same as\n")
        sys.stderr.write("the number of associated weights. Quitting.\n\n")
        sys.exit(1)

if (opts.scalefactor):
    if len(opts.scalefactor)!=len(args):
        sys.stderr.write("\n")
        sys.stderr.write("The number of aida files given is not the same as\n")
        sys.stderr.write("the number of scale factors. Quitting.\n\n")
        sys.exit(1)

if (opts.weight and opts.scalefactor):
    sys.stderr.write("\n")
    sys.stderr.write("The program cannot accept both histogram weights\n")
    sys.stderr.write("and rescaling factors - only one or the other.\n\n")
    sys.exit(1)

aidaWeights=[]
sumAidaWeights=0.0
if (opts.weight):
    for theWeight in opts.weight:
        aidaWeights.append(theWeight);
    for theWeight in opts.weight:
        sumAidaWeights+=theWeight;
    for ixx in range(0,len(aidaWeights)):
        aidaWeights[ixx]/=sumAidaWeights
    sumAidaWeights=0.0
    for ixx in range(0,len(aidaWeights)):
        sumAidaWeights+=aidaWeights[ixx];
elif (opts.scalefactor):
    for theWeight in opts.scalefactor:
        aidaWeights.append(theWeight);
else:
    for ixx in args:
        aidaWeights.append(1.0);

## Get histos
inhistos = {}
aidaIdx=-1
for aidafile in args:
    aidaIdx+=1
    print "Processing file",aidafile,"with weight",aidaWeights[aidaIdx]
    if (opts.weight or opts.scalefactor):
        try:
            outdat = open(aidafile.replace(".aida", "_ren.dat"), "w")
        except:
            sys.stderr.write("\nCouldn't open "+
                             aidafile.replace(".aida", "_ren.dat")+
                             " for writing. ")
        try:
            outaida = open(aidafile.replace(".aida", "_ren.aida"), "w")
        except:
            sys.stderr.write("\nCouldn't open "+
                             aidafile.replace(".aida", "_ren.aida")+
                             " for writing. ")
    tree = ET.parse(aidafile)
    for dps in tree.findall("dataPointSet"):
        h = lighthisto.Histo.fromDPS(dps)
        if not inhistos.has_key(h.fullPath()):
            inhistos[h.fullPath()] = {}
        if(h.getArea()>0.0):
            h=h.renormalise(aidaWeights[aidaIdx]*h.getArea())
        inhistos[h.fullPath()][aidafile] = h
        if((opts.weight or opts.scalefactor) and outdat):
            outdat.write("\n\n")
            outdat.write(h.asFlat())
            outdat.write("\n")
    if( (opts.weight or opts.scalefactor) and outdat):
        outdat.close()
        Popen(["flat2aida", aidafile.replace(".aida", "_ren.dat")],
              stdout=outaida).wait()


### Create files for the merged.dat and merged.aida result.
try:
    outaida = open(opts.OUTFILE, "w")
except:
    sys.stderr.write("Couldn't open outfile %s for writing." % opts.OUTFILE)

try:
    outdat = open(opts.OUTFILE.replace(".aida", ".dat"), "w")
except:
    sys.stderr.write("Couldn't open outfile %s for writing." % opts.OUTFILE.replace(".aida", ".dat"))

## Merge histos
outhistos = {}
for path, hs in inhistos.iteritems():
    #print path, hs
    outhistos[path] = copy.deepcopy(hs.values()[0])
    for i, b in enumerate(outhistos[path].getBins()):
        sum_val = 0.0
        sum_err2 = 0.0
        n = 0
        for infile, h in hs.iteritems():
            sum_val += h.getBin(i).val
            try:
                sum_err2 += h.getBin(i).getErr()**2
            except OverflowError:
                # in case the **2 produces overflow errors
                # set sum to 'inf'
                sum_err2 = float('inf')
            n += 1
        outhistos[path].getBin(i).val = sum_val
        try:
            outhistos[path].getBin(i).setErr(sum_err2**0.5)
        except OverflowError:
            # to get back to numerics, replace an eventual 'inf' 
            # in sum_err2 with max float available ~1.e+308
            outhistos[path].getBin(i).setErr(1.e308)

## Write out merged histos
#print sorted(outhistos.values())
outdat.write("\n\n".join([h.asFlat() for h in sorted(outhistos.values())]))
outdat.write("\n")
outdat.close()

Popen(["flat2aida", opts.OUTFILE.replace(".aida", ".dat")], stdout=outaida).wait()

os.unlink(opts.OUTFILE.replace(".aida", ".dat"))
