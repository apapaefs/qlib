#! /usr/bin/env python
from __future__ import with_statement
import cmath, string, os, sys, fileinput, pprint, math
from optparse import OptionParser
import scipy
import subprocess
from scipy import special
from scipy import stats
import scipy.stats
import numpy as np
from scipy.stats import norm
from scipy.stats import invgamma
import random
import sys
import time
#import datetime2
import os.path

parser = OptionParser(usage="%prog gen or run")

opts, args = parser.parse_args()

print 'calculate the sigmas given the chi^2 and degrees of freedom [=number of points-1]\n'

if len(args) < 2:
    print 'usage: chisq_to_sigmas [chisq] [ndof]'
    exit()

chisquared = float(args[0])
degrees_of_freedom = float(args[1])
print "sigmas for chi-squared, dof=", chisquared, degrees_of_freedom
p = stats.chi2.cdf(chisquared, degrees_of_freedom)
sigmas = scipy.stats.norm.ppf(1-(1-p)/2)
print "sigma=", sigmas, "for p-value=", p
