import scipy

signif = 1.0
print('p-value corresponding to a significance of ', signif, '=', 1-2*scipy.special.ndtr(-1.0*signif))
