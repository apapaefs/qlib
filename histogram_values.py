import sys
import pylab as pl
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colors

if len(sys.argv) < 2:
    print('histogram_values.py [input file single column format] [number of bins]')

inputfile = str(sys.argv[1])

set_n_bins = False
if len(sys.argv) > 2:
    n_bins_toset = int(sys.argv[2])
    set_n_bins = True

print('Reading', inputfile)

# Histogram values from a column file
datalist = [ ( pl.loadtxt(inputfile), 'input' ) ]
for data, label in datalist:
    x = data

#rint(x)
n_bins=20
if set_n_bins is True:
    n_bins = n_bins_toset

fig, axs = plt.subplots(1, 1, sharey=True, tight_layout=True)
axs.hist(x, bins=n_bins)

#plt.show()

# save the figure in PDF format
plt.savefig(inputfile + '.pdf', bbox_inches='tight')
